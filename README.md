# TutBase - Angular App #

Aplikacja internetowa do przeglądania i tworzenia poradników (lifehacków) z różnych dziedzin.

**Autor**: Szymon Karolczuk

### Specyfikacja ###

* Angular CLI: 11.2.3
* Node: 14.16.0

### Serwer testowy ###
<http://tutbase.szymonk.pl>
