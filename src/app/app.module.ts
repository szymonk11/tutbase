import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EditorModule } from '@tinymce/tinymce-angular';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { IvyGalleryModule } from 'angular-gallery';
import { RecaptchaFormsModule, RecaptchaModule, RecaptchaSettings, RECAPTCHA_SETTINGS } from 'ng-recaptcha';

import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from './shared/shared.module';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';

import { CategoriesComponent } from './categories/categories.component';
import { HomeComponent } from './home/home.component';
import { CategoryIndexComponent } from './categories/category-index/category-index.component';
import { TutorialIndexComponent } from './tutorials/tutorial-index/tutorial-index.component';
import { TutorialAddComponent } from './tutorials/tutorial-add/tutorial-add.component';
import { CommentsListComponent } from './tutorials/comments-list/comments-list.component';
import { SearchByTagComponent } from './search/search-by-tag/search-by-tag.component';
import { CommentAddComponent } from './tutorials/comment-add/comment-add.component';
import { RegisterComponent } from './register/register.component';
import { TutorialsFavoriteComponent } from './tutorials/tutorials-favorite/tutorials-favorite.component';
import { RegisterConfirmComponent } from './register/register-confirm/register-confirm.component';
import { SearchComponent } from './search/search.component';
import { UserProfileComponent } from './user/user-profile/user-profile.component';
import { AdminIndexComponent } from './admin/admin-index/admin-index.component';
import { ResetPasswordComponent } from './user/reset-password/reset-password.component';
import { RemindPasswordComponent } from './user/remind-password/remind-password.component';
import { TutorialEditComponent } from './tutorials/tutorial-edit/tutorial-edit.component';
import { TutorialFormComponent } from './tutorials/tutorial-form/tutorial-form.component';
import { TutorialsListComponent } from './tutorials/tutorials-list/tutorials-list.component';
import { CommentsGalleryComponent } from './tutorials/comments-gallery/comments-gallery.component';
import { UserPreviewComponent } from './user/user-preview/user-preview.component';

@NgModule({
	declarations: [
		AppComponent,
		LoginComponent,
		CategoriesComponent,
		HomeComponent,
		CategoryIndexComponent,
		TutorialIndexComponent,
		TutorialAddComponent,
		CommentsListComponent,
		SearchByTagComponent,
		CommentAddComponent,
		RegisterComponent,
		TutorialsFavoriteComponent,
		RegisterConfirmComponent,
		SearchComponent,
		UserProfileComponent,
		AdminIndexComponent,
		ResetPasswordComponent,
		RemindPasswordComponent,
		TutorialEditComponent,
		TutorialFormComponent,
		TutorialsListComponent,
		CommentsGalleryComponent,
		UserPreviewComponent
	],
	imports: [
		BrowserModule,
		HttpClientModule,
		AppRoutingModule,
		NgbModule,
		ReactiveFormsModule,
		SharedModule,
		EditorModule,
		BrowserAnimationsModule,
		AutoCompleteModule,
		IvyGalleryModule,
		RecaptchaModule,
		RecaptchaFormsModule
	],
	providers: [
		{
			provide: RECAPTCHA_SETTINGS,
			useValue: { siteKey: "6Lfb3KocAAAAAAkBjRN0lAlOomcU_CENPm7xENtb" } as RecaptchaSettings
		}
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
