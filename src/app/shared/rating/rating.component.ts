import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
	selector: 'app-rating',
	templateUrl: './rating.component.html',
	styles: [`
		.star {
		font-size: 1.5rem;
		color: #d7d7d7;
		}
		.filled {
		color:#FF9000;
		}
		.star.filled.success {
			color: #4eb54b !important;
		}
		.star.filled.danger {
			color: #BF0A00 !important;
		}
  	`]
})
export class RatingComponent {

	@Input()
	readonly = false;

	@Input()
	max = 5;

	@Input()
	rate: number;

	@Input()
	adClass: string;

	@Output()
	rateChange = new EventEmitter<number>();

	constructor() {}

	onRateChange() {
		this.rateChange.emit(this.rate);
	}
}
