import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Pipe({
	name: 'videoSrcSafe'
})
export class VideoSrcSafePipe implements PipeTransform {

    constructor(
        protected sanitizer: DomSanitizer
    ) { }

	transform(value: any): SafeResourceUrl {
		return this.sanitizer.bypassSecurityTrustResourceUrl(value);
	}

}
