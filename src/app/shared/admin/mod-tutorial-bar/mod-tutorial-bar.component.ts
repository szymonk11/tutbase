import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ApiResult } from 'src/app/core/models/ApiResult';
import { LoggedInUser } from 'src/app/core/models/LoggedInUser';
import { Tutorial, TutorialStatus } from 'src/app/core/models/Tutorial';
import { AdminService } from 'src/app/core/services/admin.service';
import { AuthService } from 'src/app/core/services/auth.service';
import { ToastsService } from 'src/app/core/services/toasts.service';

@Component({
	selector: 'app-mod-tutorial-bar',
	templateUrl: './mod-tutorial-bar.component.html'
})
export class ModTutorialBarComponent {

	@Input()
	tutorial: Tutorial;

	@Output()
	onReloadTutorial = new EventEmitter<void>();

	user: LoggedInUser;

	tutorialStatus = TutorialStatus;

	constructor(
		private authService: AuthService,
		private adminService: AdminService,
		private toastsService: ToastsService
	) { 
		this.authService.loggedInUser$.subscribe(
            (loggedInUser: LoggedInUser) => {
                this.user = loggedInUser;
            },
            () => this.user = null
        );
	}

	onClickAcceptBtn(): void
	{
		if(confirm("Czy na pewno chcesz zaakceptować ten materiał?")) {
			this.setStatus(TutorialStatus.Verified);
		}
	}

	onClickRejectBtn(): void
	{
		let reason = this.getReason();
		if(reason.length <= 0) {
			this.toastsService.warning("Musisz podać powód odrzucenia poradnika.");
			return;
		}

		if(confirm("Czy na pewno chcesz odrzucić/usunąć ten materiał? Powód: " + reason)) {
			this.setStatus(TutorialStatus.Rejected, reason);
		}
	}

	onClickForImprovementBtn(): void
	{
		let reason = this.getReason();
		if(reason.length <= 0) {
			this.toastsService.warning("Musisz podać powód odrzucenia poradnika.");
			return;
		}

		if(confirm("Czy na pewno chcesz wysłać ten materiał do autora w celu poprawy? Powód: " + reason)) {
			this.setStatus(TutorialStatus.ForImprovement, reason);
		}
	}

	private setStatus(status: TutorialStatus, reason: string = null): void
	{
		this.adminService.setStatus(this.tutorial.id, status, reason).subscribe(
			(result: ApiResult) => {
				if(result.status == 1) {
					this.toastsService.success(result.message);
					this.onReloadTutorial.emit();
				} else {
					this.toastsService.error(result.message);
				}
			}
		);
	}

	private getReason(): string
	{
		return prompt("Podaj powód odrzucenia materiału: ").trim();
	}

}
