import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ApiResult } from 'src/app/core/models/ApiResult';
import { LoggedInUser } from 'src/app/core/models/LoggedInUser';
import { AdminService } from 'src/app/core/services/admin.service';
import { AuthService } from 'src/app/core/services/auth.service';
import { ToastsService } from 'src/app/core/services/toasts.service';

@Component({
	selector: 'app-mod-comment',
	templateUrl: './mod-comment.component.html'
})
export class ModCommentComponent {

	@Input()
	commentid: number;

	@Input()
	commentContent: string;

	@Output()
	onChangeCommentsList = new EventEmitter<void>();

	user: LoggedInUser;

	constructor(
		private authService: AuthService,
		private adminService: AdminService,
		private toastsService: ToastsService
	) { 
		this.authService.loggedInUser$.subscribe(
            (loggedInUser: LoggedInUser) => {
                this.user = loggedInUser;
            },
            () => this.user = null
        );
	}

	onClickDeleteCommentBtn(): void
	{
		if(confirm('Czy na pewno chcesz usunąć wybrany komentarz? Treść: "'+ (this.commentContent ?? '') +'"')) {
			this.adminService.deleteComment(this.commentid).subscribe(
				(result: ApiResult) => {
					if(result.status == 1) {
						this.toastsService.success(result.message);
						this.onChangeCommentsList.emit();
					} else {
						this.toastsService.error(result.message);
					}
				}	
			);
		}
	}

}
