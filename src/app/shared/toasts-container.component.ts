import { Component, TemplateRef } from '@angular/core';
import { ToastsService } from '../core/services/toasts.service';

@Component({
	selector: 'app-toasts',
	template: `
		<ngb-toast
		*ngFor="let toast of toastsService.toasts"
		[class]="toast.classname"
		[autohide]="true"
		[delay]="toast.delay || 5000"
		(hidden)="toastsService.remove(toast)"
		>
		<ng-template [ngIf]="isTemplate(toast)" [ngIfElse]="text">
		<ng-template [ngTemplateOutlet]="toast.textOrTpl"></ng-template>
		</ng-template>

		<ng-template #text>{{ toast.textOrTpl }}</ng-template>
		</ngb-toast>
  	`,
	host: { '[class.ngb-toasts]': 'true' }
})
export class ToastsContainerComponent {

	constructor(public toastsService: ToastsService) { }

	isTemplate(toast) { return toast.textOrTpl instanceof TemplateRef; }

}
