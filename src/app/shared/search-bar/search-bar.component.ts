import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { Params, Router } from '@angular/router';
import { Category } from 'src/app/core/models/Category';
import { ExtraField, ExtraFieldType } from 'src/app/core/models/ExtraField';
import { TutorialType } from 'src/app/core/models/Tutorial';
import { CategoriesService } from 'src/app/core/services/categories.service';

@Component({
	selector: 'app-search-bar',
	templateUrl: './search-bar.component.html'
})
export class SearchBarComponent implements OnInit {

    tutorialTypes = TutorialType;
    extraFieldTypes = ExtraFieldType;
    searchCategories: Category[] = [];
    extraFields: ExtraField[] = [];
    searchForm = new FormGroup({
        text: new FormControl(''),
        categoryid: new FormControl('', [Validators.pattern("^[0-9]*$")]),
        type: new FormControl(''),
        language: new FormControl(''),
        extrafields: new FormArray([

        ])
    });

	constructor(
		private categoriesService: CategoriesService,
        private router: Router
	) { }

	ngOnInit(): void {
		this.categoriesService.getAll().subscribe(
            (categories: Category[]) => {
                this.searchCategories = categories;
            }
        );
	}

	onSubmitSearchForm(): void {

        let values = this.searchForm.value;

        let routeParams: Params = {};
        if(values.text.trim() !== "") {
            routeParams['slug'] = values.text.trim();
        }

        if(parseInt(values.categoryid)) {
            routeParams['categoryid'] = values.categoryid;
        }

        if(parseInt(values.type)) {
            routeParams['type'] = values.type;
        }

        if(values.language != "") {
            routeParams['language'] = values.language;
        }

        // extra fields
        values.extrafields.forEach((value, key) => {
            if(value.trim() != "" && this.extraFields[key]) {
                routeParams[ 'extra_' + this.extraFields[key].id ] = value;
            }
        });

        if(Object.keys(routeParams).length !== 0) {
            this.router.navigate(['/tutorials/search'], {queryParams: routeParams});
        }
    }

    onClearSearchForm(): void {
        this.searchForm.controls['text'].setValue('');
        this.searchForm.controls['categoryid'].setValue('');
        this.searchForm.controls['type'].setValue('');
        this.searchForm.controls['language'].setValue('');
        this.onChangeCategory('');
    }

    // extra fields
    get formExtrafields(): FormArray {
        return this.searchForm.get('extrafields') as FormArray;
    }

    onChangeCategory(categoryid): void {
        if(categoryid == "") {
            this.extraFields = [];
            this.formExtrafields.clear();
            return;
        }

        this.categoriesService.getExtraFieldsByCategory(categoryid).subscribe(
            (extraFields: ExtraField[]) => {
                this.extraFields = extraFields;

                this.formExtrafields.clear();

                extraFields.forEach((extraField) => {
                    this.formExtrafields.push(new FormControl(""));
                });
            }
        );
    }

}
