import { Component, Input } from '@angular/core';
import { Tutorial } from 'src/app/core/models/Tutorial';
import { environment } from 'src/environments/environment';

@Component({
	selector: 'app-tutorial-card-video',
	templateUrl: './tutorial-card-video.component.html',
})
export class TutorialCardVideoComponent {

	@Input()
	tutorial: Tutorial;

	files_url = environment.api_files_url + 'tutorials/';

	constructor() { }
}
