import { Component, OnInit } from '@angular/core';
import { ApiResult } from 'src/app/core/models/ApiResult';
import { AuthService } from 'src/app/core/services/auth.service';

@Component({
	selector: 'app-statistics',
	templateUrl: './statistics.component.html'
})
export class StatisticsComponent implements OnInit {

	active_tutorials_cnt = "...";
	newest_user_id = "";
	newest_user_username = "...";
	users_cnt = "...";

	constructor(
		private authService: AuthService
	) { }

	ngOnInit(): void {
		this.authService.getStatistics().subscribe(
			(result: ApiResult) => {
				if(result.status == 1) {
					this.active_tutorials_cnt = result.data.active_tutorials_cnt;
					this.newest_user_id = result.data.newest_user_id;
					this.newest_user_username = result.data.newest_user_username;
					this.users_cnt = result.data.users_cnt;
				}
			}
		);
	}

}
