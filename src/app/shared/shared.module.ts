import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { VideoSrcSafePipe } from './pipes/video-src-safe.pipe';
import { ToastsContainerComponent } from './toasts-container.component';
import { SafeHtmlPipe } from './pipes/safe-html.pipe';
import { TutorialCardComponent } from './tutorial-card/tutorial-card.component';
import { RatingComponent } from './rating/rating.component';
import { FavoriteBtnComponent } from './favorite-btn/favorite-btn.component';
import { PageContactComponent } from './pages/page-contact/page-contact.component';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { ReactiveFormsModule } from '@angular/forms';
import { TutorialCardVideoComponent } from './tutorial-card-video/tutorial-card-video.component';
import { ModTutorialBarComponent } from './admin/mod-tutorial-bar/mod-tutorial-bar.component';
import { ModCommentComponent } from './admin/mod-comment/mod-comment.component';
import { SortFormComponent } from './sort-form/sort-form.component';
import { StatisticsComponent } from './statistics/statistics.component';

@NgModule({
	declarations: [
		PageNotFoundComponent,
		VideoSrcSafePipe,
		ToastsContainerComponent,
		SafeHtmlPipe,
		TutorialCardComponent,
		RatingComponent,
		FavoriteBtnComponent,
		PageContactComponent,
		SearchBarComponent,
		TutorialCardVideoComponent,
		ModTutorialBarComponent,
		ModCommentComponent,
		SortFormComponent,
		StatisticsComponent
	],
	imports: [
		CommonModule,
		RouterModule,
		NgbModule,
		ReactiveFormsModule
	],
	exports: [
		PageNotFoundComponent,
		VideoSrcSafePipe,
		ToastsContainerComponent,
		SafeHtmlPipe,
		TutorialCardComponent,
		RatingComponent,
		FavoriteBtnComponent,
		PageContactComponent,
		SearchBarComponent,
		TutorialCardVideoComponent,
		ModTutorialBarComponent,
		ModCommentComponent,
		SortFormComponent,
		StatisticsComponent
	]
})
export class SharedModule { }
