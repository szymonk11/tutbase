import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
    selector: 'app-sort-form',
    templateUrl: './sort-form.component.html'
})
export class SortFormComponent implements OnInit {

    @Input()
    startValue: string = 'added';

    @Output()
    onValueChange = new EventEmitter<string>();

    constructor() { }

    ngOnInit(): void {
    }

    onChange(ev): void {
        this.onValueChange.emit(ev.target.value);
    }

}
