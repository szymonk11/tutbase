import { Component, Input, OnInit } from '@angular/core';
import { Tutorial } from 'src/app/core/models/Tutorial';
import { environment } from 'src/environments/environment';

@Component({
	selector: 'app-tutorial-card',
	templateUrl: './tutorial-card.component.html'
})
export class TutorialCardComponent implements OnInit {

	@Input()
	tutorial: Tutorial;

	@Input()
	editButton: boolean = false;

	@Input()
	reason: boolean = true;

	files_url = environment.api_files_url + 'tutorials/';
	extra_fields;

	constructor() {}

	ngOnInit() {
		this.extra_fields = this.tutorial ? JSON.parse(this.tutorial.extra_fields) : null;
	}
}
