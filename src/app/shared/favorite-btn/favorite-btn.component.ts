import { Component, Input, ViewChild } from '@angular/core';
import { ApiResult } from 'src/app/core/models/ApiResult';
import { LoggedInUser } from 'src/app/core/models/LoggedInUser';
import { AuthService } from 'src/app/core/services/auth.service';
import { ToastsService } from 'src/app/core/services/toasts.service';
import { TutorialsService } from 'src/app/core/services/tutorials.service';

@Component({
	selector: 'app-favorite-btn',
	templateUrl: './favorite-btn.component.html',
	styleUrls: ['./favorite-btn.component.css']
})
export class FavoriteBtnComponent  {

	@Input()
	tutorialid: number;

	@Input()
	favorite: boolean;

	private lock = false;

	constructor(
		private tutorialsService: TutorialsService,
		private authService: AuthService,
		private toastsService: ToastsService
	) { }

	onClick(ev: MouseEvent) {
		if(this.lock) return;
		this.lock = true;
		
		let element = ev.target as HTMLElement;

		this.authService.isLoggedIn().subscribe(
			(apiResult: ApiResult) => {
				if(apiResult.status == 1) {
					let loggedInUser = apiResult.data as LoggedInUser;

					if(element.classList.contains('fill')) {
						element.classList.remove('fill');
						element.classList.add('bi-heart');
						element.classList.remove('bi-heart-fill');

						this.tutorialsService.deleteFavorite(loggedInUser.id, this.tutorialid).subscribe(
							(apiResult: ApiResult) => {
								if(apiResult.status == 1) {
									this.toastsService.warning(apiResult.message);
								} else {
									this.toastsService.error(apiResult.message);
								}
							}
						);

					} else {
						element.classList.add('fill');
						element.classList.add('bi-heart-fill');
						element.classList.remove('bi-heart');

						this.tutorialsService.addFavorite(loggedInUser.id, this.tutorialid).subscribe(
							(apiResult: ApiResult) => {
								if(apiResult.status == 1) {
									this.toastsService.success(apiResult.message);
								} else {
									this.toastsService.error(apiResult.message);
								}
							}
						);
					}	
				}
				else {
					this.toastsService.error("Zostałeś wylogowany. Zaloguj się ponownie, aby dodać do listy ulubionych!");
				}
			},
			(err) => {},
			() => {
				this.lock = false;
			}
		);
	}

}
