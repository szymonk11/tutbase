import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Category } from '../core/models/Category';
import { CategoriesService } from '../core/services/categories.service';

@Injectable({
    providedIn: 'root'
})
export class CategoryResolver implements Resolve<Category> {

    constructor(
        private categoriesService: CategoriesService,
        private router: Router
    ) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
		return this.categoriesService.get(parseInt(route.paramMap.get('id')))
			.pipe(catchError((err) => this.router.navigate(['/404', 'Wybrana kategoria nie istnieje.'])));
	}
}
