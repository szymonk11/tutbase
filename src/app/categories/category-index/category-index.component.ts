import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiResult } from 'src/app/core/models/ApiResult';
import { Category } from 'src/app/core/models/Category';
import { Tutorial } from 'src/app/core/models/Tutorial';
import { CategoriesService } from 'src/app/core/services/categories.service';
import { TitleService } from 'src/app/core/services/title.service';
import { TutorialsService } from 'src/app/core/services/tutorials.service';

@Component({
    selector: 'app-category-index',
    templateUrl: './category-index.component.html'
})
export class CategoryIndexComponent implements OnInit {

    category: Category;
    subCategories: Category[];
    tutorials: Tutorial[];
    page: number = 1;
    pageSize: number = 6;
  	tutorialsSize: number = 10;
    sort: string = null;

    constructor(
        private categoriesService: CategoriesService,
        private tutorialsService: TutorialsService,
        private route: ActivatedRoute,
        private titleService: TitleService
    ) { }

    ngOnInit(): void {
        this.route.data.subscribe((data: { category: Category }) => {
            this.category = data.category;
            this.loadSubCategories();
            this.checkTutorialsSize();
            this.loadTutorials();
            this.titleService.setTitle(this.category.name);
        });
    }

    public onPageChange(): void 
    {
        this.loadTutorials();
    }

    public onChangeSort(sort: string): void
    {
        this.sort = sort;
        this.loadTutorials();
    }

    private loadSubCategories(): void
    {
        this.categoriesService.getSubCategories(this.category.id).subscribe(
            (subCategories: Category[]) => {
                this.subCategories = subCategories;
            }
        )
    }

    private loadTutorials(): void
    {
        this.tutorials = null;
        this.tutorialsService.getByCategory(this.category.id, this.page, this.pageSize, this.sort).subscribe(
            (tutorials: Tutorial[]) => {
                this.tutorials = tutorials;
            }
        )
    }

    private checkTutorialsSize(): void
    {
        this.tutorialsService.countByCategory(this.category.id).subscribe(
            (result: ApiResult) => {
                if(result.status == 1) {
                    this.tutorialsSize = result.data.cnt;
                }
            }
        )
    }

}
