import { Component, OnInit } from '@angular/core';
import { CategoriesService } from '../core/services/categories.service';
import { Category } from '../core/models/Category';

@Component({
    selector: 'app-categories',
    templateUrl: './categories.component.html',
    styles: [
    ]
})
export class CategoriesComponent implements OnInit {

    categories: Category[];

    constructor(
        private categoriesService: CategoriesService
    ) {

        categoriesService.getAll().subscribe(
            (categories: Category[]) => {
                this.categories = categories;
            }
        )

    }

    ngOnInit(): void {
    }

}
