import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { AuthService } from './core/services/auth.service';
import { LoggedInUser } from './core/models/LoggedInUser';
import { ActivatedRoute, NavigationEnd, Params, Router } from '@angular/router';
import { filter, map } from 'rxjs/operators';
import { TitleService } from './core/services/title.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

    @ViewChild('navbartoggler') navbarToggler: ElementRef; 
    
    user: LoggedInUser;

    isMenuCollapsed = true;
    isSearchCollapsed = true;

    constructor(
        private titleService: TitleService,
        private authService: AuthService,
        private router: Router,
        private route: ActivatedRoute
    ) {
        this.authService.loggedInUser$.subscribe(
            (loggedInUser: LoggedInUser) => {
                this.user = loggedInUser;
            },
            () => this.user = null
        );
    }

    ngOnInit() {
        this.router.events.pipe(
            filter(event => event instanceof NavigationEnd),
            map(() => {
                const child = this.route.firstChild;
                if(child.snapshot.data['title']) {
                    return child.snapshot.data['title'];
                }
                return "";
            })
        ).subscribe((ttl: string) => {
            this.titleService.setTitle(ttl);
        });
    }

    logout(): void {
        this.authService.logout();
    }

    clickNavbar(): void {
        if(this.navbarToggler.nativeElement.offsetTop != 0) {
            this.isMenuCollapsed = true;
        }
    }
}
