import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ApiResult } from 'src/app/core/models/ApiResult';
import { Category } from 'src/app/core/models/Category';
import { Tutorial, TutorialStatus } from 'src/app/core/models/Tutorial';
import { AdminService } from 'src/app/core/services/admin.service';
import { CategoriesService } from 'src/app/core/services/categories.service';
import { ToastsService } from 'src/app/core/services/toasts.service';

@Component({
	selector: 'app-admin-index',
	templateUrl: './admin-index.component.html',
	providers: [AdminService]
})
export class AdminIndexComponent implements OnInit {

	pendingTutorials: Tutorial[];

	categories: Category[];

	selectedCategory: Category;
	editCategoryForm = new FormGroup({
        category_name: new FormControl(''),
        parent_category: new FormControl('', [Validators.pattern("^[0-9]*$")])
    });
	createCategoryForm = new FormGroup({
		category_name: new FormControl(''),
        parent_category: new FormControl('', [Validators.pattern("^[0-9]*$")])
	});

	constructor(
		private router: Router,
		private route: ActivatedRoute,
		private modalService: NgbModal,
		private toastsService: ToastsService,
		private adminService: AdminService,
		private categoriesService: CategoriesService
	) { }

	ngOnInit(): void {
		this.route.data.subscribe((data: { isadmin: ApiResult }) => {
			if(data.isadmin.status != 1 || data.isadmin.data != 1) {
				this.router.navigate(['/']);
			} else {
				this.loadTutorials();
			}
		});
	}

	onNavChange(active: string): void
	{
		switch(active) {
			case 'categoriesNavItem':
				this.loadCategories();
				break;
		}
	}

	onEditCategoryClick(editModal, category: Category): void
	{
		this.selectedCategory = category;
		this.editCategoryForm.get('category_name').setValue(category.name);
		this.editCategoryForm.get('parent_category').setValue(category.parent_category ?? '');

		this.modalService.open(editModal).result.then((result) => {
			if(result == 'Save') {
				this.adminService.updateCategory(category.id, this.editCategoryForm.value.category_name, this.editCategoryForm.value.parent_category).subscribe(
					(result: ApiResult) => {
						this.toastsService.showResult(result);
					}
				);
			}
		}, (reason) => {});
	}

	onDeleteCategoryClick(category: Category): void
	{
		if(confirm(`Czy na pewno chcesz usunąć wybraną kategorię - ${category.name}?`)) {
			this.adminService.deleteCategory(category.id).subscribe(
				(result: ApiResult) => {
					this.toastsService.showResult(result);
				}
			)
		}
	}

	onCreateCategoryClick(): void
	{
		let category_name = this.createCategoryForm.value.category_name;
		let parent_category = this.createCategoryForm.value.parent_category;

		if(category_name.length > 0) {
			this.createCategoryForm.reset();
			this.adminService.addCategory(category_name, parent_category).subscribe(
				(result: ApiResult) => {
					this.toastsService.showResult(result);
				}
			);
		}
	}

	private loadTutorials(): void
	{
		this.adminService.getTutorialsByStatus(TutorialStatus.Pending).subscribe(
			(tutorials: Tutorial[]) => {
				this.pendingTutorials = tutorials;
			}
		);
	}

	private loadCategories(): void
	{
		this.categories = null;
		this.categoriesService.categories$.subscribe(
			(categories: Category[]) => {
				this.categories = categories;
			}
		)
	}

}
