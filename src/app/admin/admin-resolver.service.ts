import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { ApiResult } from '../core/models/ApiResult';
import { AuthService } from '../core/services/auth.service';

@Injectable({
    providedIn: 'root'
})
export class AdminResolver implements Resolve<boolean> {

    constructor(
        private authService: AuthService,
        private router: Router
    ) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return this.authService.isAdmin()
            .pipe(tap((result: ApiResult) => {
                if(result.status == 0) {
                    this.router.navigate(['/']);
                }
            }));
	}
}
