import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiResult } from 'src/app/core/models/ApiResult';
import { Tutorial } from 'src/app/core/models/Tutorial';
import { UserPreview } from 'src/app/core/models/UserPreview';
import { AuthService } from 'src/app/core/services/auth.service';
import { TitleService } from 'src/app/core/services/title.service';
import { ToastsService } from 'src/app/core/services/toasts.service';
import { TutorialsService } from 'src/app/core/services/tutorials.service';

@Component({
	selector: 'app-user-preview',
	templateUrl: './user-preview.component.html'
})
export class UserPreviewComponent implements OnInit {

	userid: number;
	user: UserPreview;
	userTutorials: Tutorial[];

	constructor(
		private authService: AuthService,
		private tutorialsService: TutorialsService,
		private toastsService: ToastsService,
		private titleService: TitleService,
		private router: Router,
        private route: ActivatedRoute
	) { }

	ngOnInit(): void {
		this.userid = this.route.snapshot.params.userid;
        
		this.authService.isLoggedIn().subscribe(
			(result: ApiResult) => {
				if(result.status != 1) {
					this.router.navigate(['/login'], { queryParams: { redirect_url: '/user/preview/'+this.userid } });
				} else {
					this.loadUser();
				}
			}
		);
	}

	private loadUser(): void {
		this.authService.getUserPreview(this.userid).subscribe(
            (user: UserPreview) => {
                this.user = user;

				this.titleService.setTitle('Użytkownik - ' + this.user.username);

				this.loadUserTutorials();
            },
            (error: any) => {
                this.router.navigate(['/404', 'Nie odnaleziono wybranego użytkownika.']);
            }
        );
	}

	private loadUserTutorials(): void {
		if(!this.user) return;
		this.tutorialsService.getByUser(this.user.id).subscribe(
			(result: ApiResult) => {
				if(result.status == 1) {
					this.userTutorials = result.data as Tutorial[];
				} else {
					this.toastsService.error(result.message);
				}
			}	
		);
	}

}
