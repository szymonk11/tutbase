import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiResult } from 'src/app/core/models/ApiResult';
import { LoggedInUser } from 'src/app/core/models/LoggedInUser';
import { Tutorial } from 'src/app/core/models/Tutorial';
import { AuthService } from 'src/app/core/services/auth.service';
import { ToastsService } from 'src/app/core/services/toasts.service';
import { TutorialsService } from 'src/app/core/services/tutorials.service';

@Component({
	selector: 'app-user-profile',
	templateUrl: './user-profile.component.html'
})
export class UserProfileComponent {
	
	user: LoggedInUser;
	userTutorials: Tutorial[];

	successMessage: string;

	changePasswordForm = new FormGroup({
        password: new FormControl('', [Validators.required, Validators.minLength(6)]),
		newPassword: new FormControl('', [Validators.required, Validators.minLength(6)]),
		newPasswordConfirm: new FormControl('', Validators.required),
    }, {
		validators: this.checkPasswords.bind(this)
	});

	constructor(
		private authService: AuthService,
		private tutorialsService: TutorialsService,
		private router: Router,
		private toastsService: ToastsService
	) { 
		this.authService.isLoggedIn().subscribe(
			(result: ApiResult) => {
				if(result.status != 1) {
					this.router.navigate(['/login'], { queryParams: { redirect_url: '/user/profile' } });
				} else {
					this.authService.loggedInUser$.subscribe(
						(loggedInUser: LoggedInUser) => {
							this.user = loggedInUser;
							
							this.loadUserTutorials();
						},
						() => this.user = null
					);
				}
			}
		);
	}

	private loadUserTutorials(): void {
		if(!this.user) return;
		this.tutorialsService.getByUser(this.user.id).subscribe(
			(result: ApiResult) => {
				if(result.status == 1) {
					this.userTutorials = result.data as Tutorial[];
				} else {
					this.toastsService.error(result.message);
				}
			}	
		);
	}

	onSubmitChangePasswordClick(): void {
		if(!this.changePasswordForm.valid) {
			this.toastsService.error("Popraw dane w formularzu!");
			return;
		}

		let values = this.changePasswordForm.value;
        this.authService.changePassword(this.user.id, values.password, values.newPassword).subscribe(
            (result: ApiResult) => {
                if(result.status == 1) {
                    this.toastsService.success(result.message);
                    this.successMessage = result.message;

                } else {
                    this.toastsService.error(result.message);
                }
            }
        );
	}

	private checkPasswords(group: FormGroup) {
        const password = group.get('newPassword').value;
        const confirmPassword = group.get('newPasswordConfirm').value;

        return password === confirmPassword ? null : { notSame: true }
    }
}
