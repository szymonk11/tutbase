import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ApiResult } from 'src/app/core/models/ApiResult';
import { AuthService } from 'src/app/core/services/auth.service';
import { ToastsService } from 'src/app/core/services/toasts.service';

@Component({
	selector: 'app-reset-password',
	templateUrl: './reset-password.component.html'
})
export class ResetPasswordComponent {

	loading = false;
	confirmed = false;

	resetForm = new FormGroup({
        password: new FormControl('', [Validators.required, Validators.minLength(6)]),
		confirmPassword: new FormControl('', Validators.required),
    }, {
		validators: this.checkPasswords.bind(this)
	});

	private userid: number;
	private reset_key: string;

	constructor(
		private authService: AuthService,
		private toastsService: ToastsService,
		private route: ActivatedRoute
	) { 

		this.userid = parseInt(this.route.snapshot.params.userid);
		this.reset_key = this.route.snapshot.params.reset_key;
 
	}

	onSubmitResetForm(): void
	{
		this.loading = true;
		this.authService.resetPassword(this.userid, this.reset_key, this.resetForm.value.password).subscribe(
			(result: ApiResult) => {
				this.loading = false;
				if(result.status == 1) {
					this.confirmed = true;
					this.resetForm.reset();
				} else {
					this.toastsService.error(result.message);
				}
			}
		);
	}

	private checkPasswords(group: FormGroup) {
        const password = group.get('password').value;
        const confirmPassword = group.get('confirmPassword').value;

        return password === confirmPassword ? null : { notSame: true }
    }

}
