import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ApiResult } from 'src/app/core/models/ApiResult';
import { AuthService } from 'src/app/core/services/auth.service';
import { ToastsService } from 'src/app/core/services/toasts.service';

@Component({
	selector: 'app-remind-password',
	templateUrl: './remind-password.component.html'
})
export class RemindPasswordComponent {

	errorMessage: string = "";
    successMessage: string = "";

	loginForm = new FormGroup({
        email: new FormControl('', [Validators.required, Validators.email])
    });

	private lock = false;

	constructor(
		private authService: AuthService,
		private toastsService: ToastsService
	) { }

	onSubmitRemindPasswordForm(): void
    {
		if(this.lock) {
			return;
		}
		
        this.authService.remindPassword(this.loginForm.value.email).subscribe(
            (result: ApiResult) => {
                if(result.status == 1) {
                    this.toastsService.success(result.message);
                    this.errorMessage = "";
                    this.successMessage = result.message;

					this.lock = true;
					setTimeout(() => {
						this.lock = false;
					}, 5000);

                } else {
                    this.toastsService.error(result.message);
                    this.errorMessage = result.message;
                }
            }
        );
    }

}
