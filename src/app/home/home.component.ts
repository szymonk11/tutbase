import { Component } from '@angular/core';
import { Tutorial } from '../core/models/Tutorial';
import { Comment } from '../core/models/Comment';
import { CommentsService } from '../core/services/comments.service';
import { TutorialsService } from '../core/services/tutorials.service';
import { Tag } from '../core/models/Tag';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html'
})
export class HomeComponent {

	lastTutorials: Tutorial[] ;
	topViewsTutorials: Tutorial[];
	lastComments: Comment[];
	topTags: Tag[];

	constructor(
		private tutorialsService: TutorialsService,
		private commentsService: CommentsService
	) 
	{ 
		this.tutorialsService.getLast(2).subscribe(
			(tutorials: Tutorial[]) => {
				this.lastTutorials = tutorials;
			}
		);
		
		this.tutorialsService.getTopViews(5).subscribe(
			(tutorials: Tutorial[]) => {
				this.topViewsTutorials = tutorials;
			}
		);

		this.commentsService.getLast(3).subscribe(
			(comments: Comment[]) => {
				this.lastComments = comments;
			}
		);

		this.tutorialsService.getTopTags(10).subscribe(
			(tags: Tag[]) => {
				this.topTags = tags;
			}
		);
	}
}
