import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiResult } from '../core/models/ApiResult';

import { AuthService } from '../core/services/auth.service';
import { ToastsService } from '../core/services/toasts.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {

    errorMessage: string = "";
    successMessage: string = "";

    loginForm = new FormGroup({
        email: new FormControl('', [Validators.required, Validators.email]),
        password: new FormControl('', Validators.required),
        remember: new FormControl(false)
    });

    private redirect_url: string = null;

    constructor(
        private authService: AuthService,
        private toastsService: ToastsService,
        private router: Router,
        private route: ActivatedRoute
    ) { 

        //this.authService.isLoggedIn().subscribe();
    }

    ngOnInit() {
        this.redirect_url = this.route.snapshot.queryParams.redirect_url ?? null;
    }

    onSubmitLoginForm(): void
    {
        this.authService.login(this.loginForm.value.email, this.loginForm.value.password, this.loginForm.value.remember === true ? 1 : 0).subscribe(
            (result: ApiResult) => {
                if(result.status == 1) {
                    this.toastsService.success(result.message);
                    this.errorMessage = "";
                    this.successMessage = result.message;
                    setTimeout(() => {
                        this.router.navigate([ (this.redirect_url) ? this.redirect_url : '/' ]);
                    }, 500);
                } else {
                    this.toastsService.error(result.message);
                    this.errorMessage = result.message;
                }
            }
        );
    }
}
