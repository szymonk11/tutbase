import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiResult } from 'src/app/core/models/ApiResult';
import { AuthService } from 'src/app/core/services/auth.service';
import { ToastsService } from 'src/app/core/services/toasts.service';

@Component({
	selector: 'app-register-confirm',
	templateUrl: './register-confirm.component.html',
})
export class RegisterConfirmComponent {

	loading = true;
	confirmed = false;

	constructor(
		private authService: AuthService,
		private toastsService: ToastsService,
		private route: ActivatedRoute
	) { 

		let userid = this.route.snapshot.params.userid;
		let register_key = this.route.snapshot.params.register_key;

		this.authService.confirm(userid, register_key).subscribe(
			(result: ApiResult) => {
				this.loading = false;
				if(result.status == 1) {
					this.confirmed = true;
				} else {
					this.toastsService.error(result.message);
				}
			}
		);
	}
}
