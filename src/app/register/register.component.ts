import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiResult } from '../core/models/ApiResult';
import { AuthService } from '../core/services/auth.service';
import { ToastsService } from '../core/services/toasts.service';

@Component({
	selector: 'app-register',
	templateUrl: './register.component.html',
	styles: [
	]
})
export class RegisterComponent {

	errorMessage: string = "";
    successMessage: string = "";

    registerForm = new FormGroup({
        email: new FormControl('', [Validators.required, Validators.email]),
		firstname: new FormControl('', [Validators.required, Validators.minLength(3)]),
        password: new FormControl('', [Validators.required, Validators.minLength(6)]),
		confirmPassword: new FormControl('', Validators.required),
        terms: new FormControl(0, [Validators.required]),
        recaptcha: new FormControl(null, Validators.required),
    }, {
		validators: this.checkPasswords.bind(this)
	});

    constructor(
        private authService: AuthService,
        private toastsService: ToastsService,
        private router: Router
    ) { 

        this.authService.isLoggedIn().subscribe();
    }

    onSubmitRegisterForm(): void
    {
        if(!this.registerForm.valid) {
			this.toastsService.error("Popraw dane w formularzu!");
			return;
		}

        let values = this.registerForm.value;
        this.authService.register(values.email, values.password, values.firstname).subscribe(
            (result: ApiResult) => {
                if(result.status == 1) {
                    this.toastsService.success(result.message);
                    this.errorMessage = "";
                    this.successMessage = result.message;
                    setTimeout(() => {
                        this.router.navigate(['/login']);
                    }, 500);

                } else {
                    this.toastsService.error(result.message);
                    this.errorMessage = result.message;
                }
            }
        );
    }

    private checkPasswords(group: FormGroup) {
        const password = group.get('password').value;
        const confirmPassword = group.get('confirmPassword').value;

        return password === confirmPassword ? null : { notSame: true }
    }

}
