import { HttpParams } from '@angular/common/http';
import { Component, OnChanges, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { SearchResult } from '../core/models/SearchResult';
import { Tutorial } from '../core/models/Tutorial';
import { TutorialsService } from '../core/services/tutorials.service';

@Component({
	selector: 'app-search',
	templateUrl: './search.component.html'
})
export class SearchComponent implements OnInit {

	tutorials: Tutorial[];
	page: number = 1;
    pageSize: number = 10;
  	tutorialsSize: number = 0;
	params: Params;

	constructor(
		private tutorialsService: TutorialsService,
		private route: ActivatedRoute
	) { }

	ngOnInit(): void {
		this.route.queryParams.subscribe(
			(params: Params) => {
				this.params = params;
				this.loadTutorials();
			}
		);
	}

	public onPageChange(): void 
    {
        this.tutorials = null;
        this.loadTutorials();
    }

	private loadTutorials(): void 
	{
		this.tutorialsService.search(this.params, this.page, this.pageSize).subscribe(
			(result: SearchResult) => {
				this.tutorials = result.tutorials;
				this.tutorialsSize = result.total_cnt;
			}	
		);
	}
}
