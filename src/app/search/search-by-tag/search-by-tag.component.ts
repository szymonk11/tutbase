import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiResult } from 'src/app/core/models/ApiResult';
import { Tutorial } from 'src/app/core/models/Tutorial';
import { TitleService } from 'src/app/core/services/title.service';
import { TutorialsService } from 'src/app/core/services/tutorials.service';

@Component({
	selector: 'app-search-by-tag',
	templateUrl: './search-by-tag.component.html'
})
export class SearchByTagComponent implements OnInit {

	tag: string = "";
	tutorials: Tutorial[];
    page: number = 1;
    pageSize: number = 10;
  	tutorialsSize: number = 10;
	  
	constructor(
		private route: ActivatedRoute,
		private tutorialsService: TutorialsService,
		private titleService: TitleService
	) { }

	ngOnInit(): void {
		this.tag = this.route.snapshot.params.tagname;

		this.tutorialsService.countByTag(this.tag).subscribe(
			(result: ApiResult) => {
				if(result.status == 1) {
					this.tutorialsSize = result.data.cnt;
				}
			}
		);

		this.tutorialsService.getByTag(this.tag, this.page, this.pageSize).subscribe(
			(tutorials: Tutorial[]) => {
				this.tutorials = tutorials;
			}
		);

		this.titleService.setTitle("#" + this.tag);
	}
}
