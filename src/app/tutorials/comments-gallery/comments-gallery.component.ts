import { Component, Input, OnInit } from '@angular/core';
import { Gallery } from 'angular-gallery';
import { environment } from 'src/environments/environment';

@Component({
	selector: 'app-comments-gallery',
	templateUrl: './comments-gallery.component.html'
})
export class CommentsGalleryComponent implements OnInit {

	files_url = environment.api_files_url + 'comments/';
	
	@Input()
	photoJSON: string;

	files = [];

	constructor(
		private gallery: Gallery
	) { }

	ngOnInit(): void {
		if(!this.photoJSON)
			return;

		let files = JSON.parse(this.photoJSON);
		files.forEach(file => {
			this.files.push(this.files_url + file);
		});
	}

	showGallery(index: number) {
		let prop = {
			images: [],
			index
		};

		this.files.forEach(file => {
			prop.images.push({
				path: file
			});
		});

		this.gallery.load(prop);
	}

}
