import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiResult } from 'src/app/core/models/ApiResult';
import { Tutorial } from 'src/app/core/models/Tutorial';
import { TutorialsService } from 'src/app/core/services/tutorials.service';

@Component({
	selector: 'app-tutorials-list',
	templateUrl: './tutorials-list.component.html'
})
export class TutorialsListComponent implements OnInit {

    tutorials: Tutorial[];
    page: number = 1;
    pageSize: number = 6;
  	tutorialsSize: number = 10;
    sort: string = null;

	constructor(
        private tutorialsService: TutorialsService,
        private route: ActivatedRoute
    ) { }

    ngOnInit(): void {
		this.sort = this.route.snapshot.queryParams.sort ?? 'added';
        this.checkTutorialsSize();
        this.loadTutorials();
    }

	public onPageChange(): void 
    {
        this.loadTutorials();
    }

    public onChangeSort(sort: string): void
    {
        this.sort = sort;
        this.loadTutorials();
    }

    private loadTutorials(): void
    {
        this.tutorials = null;
        this.tutorialsService.getAll(this.page, this.pageSize, this.sort).subscribe(
            (tutorials: Tutorial[]) => {
                this.tutorials = tutorials;
            }
        )
    }

    private checkTutorialsSize(): void
    {
        this.tutorialsService.countAll().subscribe(
            (result: ApiResult) => {
                if(result.status == 1) {
                    this.tutorialsSize = result.data.cnt;
                }
            }
        );
    }

}
