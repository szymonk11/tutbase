import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiResult } from 'src/app/core/models/ApiResult';
import { LoggedInUser } from 'src/app/core/models/LoggedInUser';
import { NewComment } from 'src/app/core/models/NewComment';
import { AuthService } from 'src/app/core/services/auth.service';
import { CommentsService } from 'src/app/core/services/comments.service';
import { ToastsService } from 'src/app/core/services/toasts.service';

@Component({
	selector: 'app-comment-add',
	templateUrl: './comment-add.component.html',
	styles: [
	]
})
export class CommentAddComponent implements OnInit {

	accept_formats = ['.jpg', '.jpeg', '.png'];

	@Input()
	tutorialid: number;

	@Output()
	afterAddComment = new EventEmitter<number>();

	commentAddForm = new FormGroup({
        content: new FormControl("", [Validators.required])
    });

	difficulty = 0;
	usefulness = 0;

	photosFiles: File[] = [];

	constructor(
		private authService: AuthService,
		private toastsService: ToastsService,
		private commentsService: CommentsService
	) { }

	ngOnInit(): void {
	}

	onSubmitComment(): void {
		if(this.commentAddForm.valid) {
			
            this.authService.isLoggedIn().subscribe(
                (apiResult: ApiResult) => {
                    if(apiResult.status == 1) {
                        let loggedInUser = apiResult.data as LoggedInUser;

                        let values = this.commentAddForm.value;

                        let validation = true;
                        if(!this.difficulty || this.difficulty < 0 || this.difficulty > 5) {
                            validation = false;
                            this.toastsService.error("Określ trudność poradnika.");
						}
						if(!this.usefulness || this.usefulness < 0 || this.usefulness > 5) {
                            validation = false;
                            this.toastsService.error("Określ przydatność poradnika.");
						}
						if(!values.content || values.content.trim() == "") {
							validation = false;
							this.toastsService.error("Treść recenzji jest pusta. Napisz coś.");
						}

                        if(!validation) {
                            return false;
                        }
                        
						let comment: NewComment = {} as NewComment;
						comment.tutorialid = this.tutorialid;
						comment.userid = loggedInUser.id;
						comment.difficulty = this.difficulty;
						comment.usefulness = this.usefulness;
						comment.content = values.content;

						let formData = new FormData();
						formData.append('tutorialid', this.tutorialid.toString());
						formData.append('userid', loggedInUser.id.toString());
						formData.append('difficulty', this.difficulty.toString());
						formData.append('usefulness', this.usefulness.toString());
						formData.append('content', values.content);

						// formData.append('photos[]', this.photosFiles);
						this.photosFiles.forEach((file: File) => {
							formData.append('photos[]', file);
						});
                        
						this.commentsService.add(formData).subscribe(
							(apiResult: ApiResult) => {
								if(apiResult.status == 1) {
									this.toastsService.success("Recenzja została pomyślnie dodana.");
									this.difficulty = 0;
									this.usefulness = 0;
									this.commentAddForm.reset();
									this.afterAddComment.emit(1);
								}
							},
							(error: any) => {
								this.toastsService.error("Wystąpił nieoczekiwany błąd.");
							}
						);

                    } else {
                        this.toastsService.error("Zostałeś wylogowany. Zaloguj się ponownie, aby dodać recenzję!");
                    }
                },
                () => {
                    this.toastsService.error("Zostałeś wylogowany. Zaloguj się ponownie, aby dodać recenzję!");
                }
            );
        } else {
            this.toastsService.error("Wypełnij wszystkie wymagane pola.");
        }
	}

	onFileChange(event): void {
        let fileList: FileList = event.target.files;
        if(fileList.length > 0) {

			for(let i = 0; i < fileList.length; i++) {
				let file = fileList.item(i);

				if(file.size > 1024 * 1024 * 2) {
					this.toastsService.error(`Wybrany plik ${file.name} jest za duży.`);
				} else if(this.isFileExists(file)) {
				
				} else if(this.photosFiles.length >= 5) {
					this.toastsService.error("Możesz załączyć maksymalnie 5 zdjęć.");
				} else {
					this.photosFiles.push(file);
				}
			}
        }
    }

	onDeleteFile(index: number): void {
		if(this.photosFiles[index]) {
			this.photosFiles.splice(index, 1);
		}
	}

	private isFileExists(file: File): boolean {
		let exists = false;
		this.photosFiles.forEach((v) => {
			if(v.name == file.name) {
				exists = true;
			}
		});
		return exists;
	}
}
