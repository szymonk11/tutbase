import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiResult } from 'src/app/core/models/ApiResult';
import { UpdateTutorial } from 'src/app/core/models/UpdateTutorial';
import { ToastsService } from 'src/app/core/services/toasts.service';
import { TutorialsService } from 'src/app/core/services/tutorials.service';

@Component({
	selector: 'app-tutorial-edit',
	templateUrl: './tutorial-edit.component.html'
})
export class TutorialEditComponent implements OnInit {

	tutorialid: number;

	constructor(
		private router: Router,
		private route: ActivatedRoute,
		private tutorialsService: TutorialsService,
		private toastsService: ToastsService
	) { }

	ngOnInit(): void {
		this.route.data.subscribe((data: { iscanedit: ApiResult }) => {

			if (data.iscanedit.status === 1) {

				this.tutorialid = this.route.snapshot.params.id;

			} else {
				this.router.navigate(['/']);
			}
		});
	}

	onSubmitForm(tutorial: UpdateTutorial): void {
		this.tutorialsService.update(tutorial).subscribe(
            (apiResult: ApiResult) => {
                if(apiResult.status == 1) {
                    this.toastsService.success("Dane został pomyślnie zapisane i czekają na weryfikację.");
					this.router.navigate(['/tutorials/' + tutorial.id]);
                }
            },
            (error: any) => {
                this.toastsService.error("Wystąpił nieoczekiwany błąd.");
            }
        );
	}

}
