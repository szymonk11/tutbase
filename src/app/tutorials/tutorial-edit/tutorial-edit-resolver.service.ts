import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { tap } from 'rxjs/operators';
import { ApiResult } from 'src/app/core/models/ApiResult';
import { AuthService } from 'src/app/core/services/auth.service';
import { TutorialsService } from 'src/app/core/services/tutorials.service';

@Injectable({
    providedIn: 'root'
})
export class TutorialEditResolver implements Resolve<boolean> {

    constructor(
        private authService: AuthService,
        private tutorialsService: TutorialsService,
        private router: Router
    ) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {

        return this.tutorialsService.isCanEdit(route.params.id)
            .pipe(tap(
            (result: ApiResult) => {
                if(result.status !== 1) {
                    this.router.navigate(['/']);
                    return false;
                }
            }
        ));
	}
}