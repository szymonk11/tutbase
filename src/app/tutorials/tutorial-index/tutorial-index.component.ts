import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoggedInUser } from 'src/app/core/models/LoggedInUser';
import { Tutorial } from 'src/app/core/models/Tutorial';
import { AuthService } from 'src/app/core/services/auth.service';
import { TitleService } from 'src/app/core/services/title.service';
import { TutorialsService } from 'src/app/core/services/tutorials.service';
import { environment } from 'src/environments/environment';
import { CommentsListComponent } from '../comments-list/comments-list.component';

@Component({
    selector: 'app-tutorial-index',
    templateUrl: './tutorial-index.component.html'
})
export class TutorialIndexComponent implements OnInit {

    @ViewChild('commentslist') commentslist: CommentsListComponent;

    files_url = environment.api_files_url + 'tutorials/';
    tutorial: Tutorial;
    extra_fields;
    commentsCnt: number;
    isFavorite: boolean;

    user: LoggedInUser;

    private tutorialid: number;

    constructor(
        private authService: AuthService,
        private tutorialsService: TutorialsService,
        private router: Router,
        private route: ActivatedRoute,
        private titleService: TitleService
    ) { 
        this.authService.loggedInUser$.subscribe(
            (loggedInUser: LoggedInUser) => {
                this.user = loggedInUser;
            },
            () => this.user = null
        );
    }

    ngOnInit(): void {
        this.tutorialid = this.route.snapshot.params.id;
        
        this.tutorialsService.get(this.tutorialid).subscribe(
            (tutorial: Tutorial) => {
                this.tutorial = tutorial;

                this.extra_fields = JSON.parse(tutorial.extra_fields); 

                this.isFavorite = this.tutorialsService.isFavorite(tutorial.id);

                this.tutorialsService.insertView(this.tutorial.id).subscribe();

                this.titleService.setTitle(this.tutorial.title);
            },
            (error: any) => {
                this.router.navigate(['/404', 'Nie odnaleziono wybranego poradnika.']);
            }
        );
    }

    reloadTutorial(): void {
        this.tutorialsService.get(this.tutorialid).subscribe(
            (tutorial: Tutorial) => {
                this.tutorial = tutorial;

                this.extra_fields = JSON.parse(tutorial.extra_fields); 

                this.isFavorite = this.tutorialsService.isFavorite(tutorial.id);

                this.titleService.setTitle(this.tutorial.title);
            },
            (error: any) => {
                this.router.navigate(['/404', 'Nie odnaleziono wybranego poradnika.']);
            }
        );
    }

    onCommentsSizeChange(commentsCnt: number): void {
        this.commentsCnt = commentsCnt;
    }

}
