import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ApiResult } from 'src/app/core/models/ApiResult';
import { NewTutorial } from 'src/app/core/models/NewTutorial';
import { AuthService } from 'src/app/core/services/auth.service';
import { ToastsService } from 'src/app/core/services/toasts.service';
import { TutorialsService } from 'src/app/core/services/tutorials.service';
import { TutorialFormComponent } from '../tutorial-form/tutorial-form.component';

@Component({
    selector: 'app-tutorial-add',
    templateUrl: './tutorial-add.component.html'
})
export class TutorialAddComponent implements OnInit {

    @ViewChild(TutorialFormComponent) 
    tutorialFormViewChild: TutorialFormComponent;

    constructor(
        private authService: AuthService,
        private toastsService: ToastsService,
        private tutorialsService: TutorialsService,
        private router: Router
    ) { }

    ngOnInit(): void {

        this.authService.isLoggedIn().subscribe(
			(result: ApiResult) => {
				if(result.status != 1) {
					this.router.navigate(['/login'], { queryParams: { redirect_url: '/tutorials/add' } });
				}
			}
		);
    }

    onSubmitForm(tutorial: NewTutorial): void {

        this.tutorialsService.add(tutorial).subscribe(
            (apiResult: ApiResult) => {
                if(apiResult.status == 1) {
                    this.toastsService.success("Poradnik został pomyślnie wysłany do weryfikacji.");

                    if(!this.tutorialFormViewChild.uploadFile(apiResult.data.tutorialid)) {
                        this.router.navigate(['/']);
                    }
                }
            },
            (error: any) => {
                this.toastsService.error("Wystąpił nieoczekiwany błąd.");
            }
        );
    }

    onUploadComplete(apiResult: ApiResult): void {
        // nothing
    }
}
