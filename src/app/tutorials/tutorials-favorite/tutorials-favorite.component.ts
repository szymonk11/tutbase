import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiResult } from 'src/app/core/models/ApiResult';
import { LoggedInUser } from 'src/app/core/models/LoggedInUser';
import { Tutorial } from 'src/app/core/models/Tutorial';
import { AuthService } from 'src/app/core/services/auth.service';
import { TutorialsService } from 'src/app/core/services/tutorials.service';

@Component({
	selector: 'app-tutorials-favorite',
	templateUrl: './tutorials-favorite.component.html'
})
export class TutorialsFavoriteComponent implements OnInit {

	tutorials: Tutorial[];
    page: number = 1;
    pageSize: number = 5;
  	tutorialsSize: number = 5;

	private userid: number;

	constructor(
		private authService: AuthService,
		private tutorialsService: TutorialsService,
        private route: ActivatedRoute,
		private router: Router
	) { }

	ngOnInit(): void {
		this.authService.isLoggedIn().subscribe(
			(result: ApiResult) => {
				if(result.status != 1) {
					this.router.navigate(['/login'], { queryParams: { redirect_url: '/favorite' } });
				} else {
					this.userid = result.data.id;
					this.loadTutorials();
				}
			}
		);
	}

	public onPageChange(): void 
    {
        this.tutorials = null;
        this.loadTutorials();
    }

    private loadTutorials(): void
    {
		this.tutorialsService.getFavoritesByUser(this.userid, this.page, this.pageSize).subscribe(
            (tutorials: Tutorial[]) => {
                this.tutorials = tutorials;
            }
        );
    }

    private checkTutorialsSize(): void
    {
        this.tutorialsService.countFavoritesByUser(this.userid).subscribe(
            (result: ApiResult) => {
                if(result.status == 1) {
                    this.tutorialsSize = result.data.cnt;
                }
            }
        )
    }

}
