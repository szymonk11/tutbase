import { HttpEvent, HttpEventType } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiResult } from 'src/app/core/models/ApiResult';
import { Category } from 'src/app/core/models/Category';
import { ExtraField, ExtraFieldType } from 'src/app/core/models/ExtraField';
import { LoggedInUser } from 'src/app/core/models/LoggedInUser';
import { NewTutorial } from 'src/app/core/models/NewTutorial';
import { Tutorial, TutorialType } from 'src/app/core/models/Tutorial';
import { UpdateTutorial } from 'src/app/core/models/UpdateTutorial';
import { AuthService } from 'src/app/core/services/auth.service';
import { CategoriesService } from 'src/app/core/services/categories.service';
import { ToastsService } from 'src/app/core/services/toasts.service';
import { TutorialsService } from 'src/app/core/services/tutorials.service';
import { environment } from 'src/environments/environment';

@Component({
	selector: 'app-tutorial-form',
	templateUrl: './tutorial-form.component.html'
})
export class TutorialFormComponent implements OnInit {

	@Input()
	editing: boolean = false;

	@Input()
	tutorialid: number;

	@Output()
	onSubmitForm = new EventEmitter<NewTutorial|UpdateTutorial>();

	@Output()
	onUploadComplete = new EventEmitter<ApiResult>();

    tutorial: Tutorial;

	iny_api_key = environment.tiny_api_key;
    private youtube_vimeo_embed_regex: RegExp = /^(https:\/\/){1}(www\.)?((youtube\.com\/embed)|(player\.vimeo\.com\/video))\/\S+/i;
    accept_formats = ['.mp4', '.mkv'];
    max_size_mb = 50;

    tutorialTypes = TutorialType;
    extraFieldTypes = ExtraFieldType;
    categories: Category[] = [];
    extraFields: ExtraField[] = [];

    tutorialForm = new FormGroup({
        title: new FormControl("", [Validators.required, Validators.maxLength(128), Validators.minLength(6)]),
        description: new FormControl("", [Validators.required, Validators.maxLength(255)]),
        categoryid: new FormControl(null, [Validators.required, Validators.pattern("^[0-9]*$")]),
        lang: new FormControl("pl", [Validators.required]),
        type: new FormControl(1, [Validators.required]),
        content: new FormControl(""),
        url: new FormControl("", [Validators.pattern(this.youtube_vimeo_embed_regex)]),
        tags: new FormControl(""),
        extrafields: new FormArray([

        ]),
        statement: new FormControl(false, Validators.requiredTrue)
    });
    difficulty: number = 1;
    tags: string[] = [];
    tutorialFile: File = null;

    uploading = false;
    uploaded = false;
    uploadProgress = 0;

	constructor(
		private authService: AuthService,
        private categoriesService: CategoriesService,
        private toastsService: ToastsService,
        private tutorialsService: TutorialsService,
        private router: Router
	) { }

	ngOnInit(): void {
		this.categoriesService.getAll().subscribe(
			(categories: Category[]) => {
				this.categories = categories;
			}
		);

		if(this.editing && this.tutorialid) {
            this.loadTutorialData(this.tutorialid);
		}
	}

    private loadTutorialData(tutorialid: number):void
    {
        this.tutorialsService.get(this.tutorialid).subscribe(
            (tutorial: Tutorial) => {
                this.tutorial = tutorial;
                this.tutorialForm.get('title').setValue(tutorial.title);
                this.tutorialForm.get('description').setValue(tutorial.description);
                this.tutorialForm.get('categoryid').setValue(tutorial.categoryid);
                this.tutorialForm.get('lang').setValue(tutorial.lang);
                this.tutorialForm.get('type').setValue(tutorial.type);
                this.tutorialForm.get('content').setValue(tutorial.content);
                this.tutorialForm.get('url').setValue(tutorial.url);

                this.onChangeCategory(tutorial.categoryid);

                if(tutorial.tags) {
                    tutorial.tags.split(',').forEach((tag) => {
                       this.tags.push(tag);
                    });
                }

                this.difficulty = tutorial.difficulty;
            }
        );
    }

	submitFormClick(): void {

        if(this.tutorialForm.valid) {

            this.authService.isLoggedIn().subscribe(
                (apiResult: ApiResult) => {
                    if(apiResult.status == 1) {
                        let loggedInUser = apiResult.data as LoggedInUser;

                        let values = this.tutorialForm.value;

                        let tutorial: UpdateTutorial|NewTutorial;

                        if(this.editing) {

                            tutorial = {} as UpdateTutorial;
                            tutorial.id = this.tutorial.id;
                            tutorial.title = values.title;
                            tutorial.description = values.description;
                            tutorial.type = values.type;
                            tutorial.lang = values.lang;
                            tutorial.categoryid = values.categoryid;
                            tutorial.url = null;
                            tutorial.content = null;
                            tutorial.difficulty = this.difficulty;
                            tutorial.extra_fields = JSON.stringify( this.getExtraFieldsFromForm() );
                            tutorial.tags = this.tags.length > 0 ? this.tags : null;

                            let validation = true;
                            if(values.type == TutorialType.Video) {
    
                                if(!this.tutorial.filename && (!values.url || values.url.trim() == "")) {
                                    validation = false;
                                    this.toastsService.error("Podaj link do materiału wideo lub wybierz plik.");
                                }
    
                                if(values.url && values.url.trim() == "" && !this.youtube_vimeo_embed_regex.test(values.url)) {
                                    validation = false;
                                    this.toastsService.error("Błędny format adresu URL filmu.");
                                }
    
                                tutorial.url = values.url;
                            } 
                            else if(values.type == TutorialType.Text) {
    
                                if(!values.content || values.content.trim() == "") {
                                    validation = false;
                                    this.toastsService.error("Treść poradnika jest pusta. Napisz coś.");
                                }
                                
                                tutorial.content = values.content;
                            }
                            else if(values.type == TutorialType.StepByStep) {
                                validation = false;
                                this.toastsService.error("Ten typ poradnika nie jest jeszcze obsługiwany.");
                            }
                            else {
                                this.toastsService.error("Wybierz typ poradnika!");
                            }
    
                            if(!validation) {
                                return false;
                            }

                        } else {
                            tutorial = {} as NewTutorial;
                            tutorial.userid = loggedInUser.id;
                            tutorial.title = values.title;
                            tutorial.description = values.description;
                            tutorial.type = values.type;
                            tutorial.lang = values.lang;
                            tutorial.categoryid = values.categoryid;
                            tutorial.url = null;
                            tutorial.content = null;
                            tutorial.difficulty = this.difficulty;
                            tutorial.extra_fields = JSON.stringify( this.getExtraFieldsFromForm() );
                            tutorial.tags = this.tags.length > 0 ? this.tags : null;
    
                            let validation = true;
                            if(values.type == TutorialType.Video) {
    
                                if(this.tutorialFile  == null && (!values.url || values.url.trim() == "")) {
                                    validation = false;
                                    this.toastsService.error("Podaj link do materiału wideo lub wybierz plik.");
                                }
    
                                if(values.url && values.url.trim() == "" && !this.youtube_vimeo_embed_regex.test(values.url)) {
                                    validation = false;
                                    this.toastsService.error("Błędny format adresu URL filmu.");
                                }
    
                                tutorial.url = values.url;
                            } 
                            else if(values.type == TutorialType.Text) {
    
                                if(!values.content || values.content.trim() == "") {
                                    validation = false;
                                    this.toastsService.error("Treść poradnika jest pusta. Napisz coś.");
                                }
                                
                                tutorial.content = values.content;
                            }
                            else if(values.type == TutorialType.StepByStep) {
                                validation = false;
                                this.toastsService.error("Ten typ poradnika nie jest jeszcze obsługiwany.");
                            }
                            else {
                                this.toastsService.error("Wybierz typ poradnika!");
                            }
    
                            if(!validation) {
                                return false;
                            }
                        }

						this.onSubmitForm.emit(tutorial);

                    } else {
                        this.toastsService.error("Zostałeś wylogowany. Zaloguj się ponownie, aby dodać poradnik!");
                    }
                },
                () => {
                    this.toastsService.error("Zostałeś wylogowany. Zaloguj się ponownie, aby dodać poradnik!");
                }
            );
        } else {
            this.toastsService.error("Popraw dane w formularzu.");
        }
    }

	uploadFile(tutorialid: number): boolean {
		if(this.tutorialForm.value.type == TutorialType.Video && this.tutorialFile != null) {
			this.tutorialsService.uploadVideo(this.tutorialFile).subscribe(
				(event: HttpEvent<any>) => {
					switch(event.type) {
						case HttpEventType.Sent:
							//console.log('Request has been made!');
							this.uploading = true;
							break;
						case HttpEventType.ResponseHeader:
							//console.log('Response header has been received!');
							break;
						case HttpEventType.UploadProgress:
							this.uploadProgress = Math.round(event.loaded / event.total * 100);
							//console.log(`Uploaded! ${this.uploadProgress}%`);
							break;
						case HttpEventType.Response:
							this.onUploadCompleted(tutorialid, event.body);
							//console.log('User successfully created!', event.body);
						   break; 
					}
				}  
			);
			return true; // upload wystartował
		} else {
			return false; // nic do wrzucenia
		}
	}

	onFileChange(event): void {
        let fileList: FileList = event.target.files;
        if(fileList.length > 0) {
            let file = fileList[0];
            if(file.size > 1024 * 1024 * 50) {
                this.toastsService.error("Wybrany plik jest za duży.");
            } else {
                this.tutorialFile = file;
            }
            
        } else {
            this.tutorialFile = null;
        }
    }

    private onUploadCompleted(tutorialid: number, result: ApiResult): void {
        this.uploaded = true;

        // save FILENAME to TUTORIAL
        this.tutorialsService.afterUploaded(tutorialid, result.data.filename).subscribe();

		this.onUploadComplete.emit(result);
    }

    // extra fields
    get formExtrafields() {
        return this.tutorialForm.get('extrafields') as FormArray;
    }

    getExtraFieldsFromForm() {
        let length = 0;
        let extrafields = {};
        let formValues = this.tutorialForm.value.extrafields;
        formValues.forEach((value, key) => {
            if(value.trim() != "" && this.extraFields[key]) {
                extrafields[ this.extraFields[key].name ] = value;
                length++;
            }
        });
        return length ? extrafields : null;
    }

    onChangeCategory(categoryid) {

        let extra_values = [];
        if(this.tutorial && this.tutorial.extra_fields) {
            extra_values = JSON.parse(this.tutorial.extra_fields);
        }

        this.categoriesService.getExtraFieldsByCategory(categoryid).subscribe(
            (extraFields: ExtraField[]) => {
                this.extraFields = extraFields;

                this.formExtrafields.clear();

                let value = "";
                extraFields.forEach((extraField) => {
                    value = extra_values && extra_values[extraField.name] ? extra_values[extraField.name] : "";
                    this.formExtrafields.push(new FormControl(value ?? ""));
                });
            }
        );
    }
    // ===

    // tags
    onKeyUp(event: KeyboardEvent) {
        if (event.key == "Enter" || event.key == "," || event.key == ";") {
            let input = event.target as any;

            let newTag = input.value.trim().toLowerCase();
            newTag = newTag.replaceAll(/[\,\;\.\/\"\"\'\'\s]/g, "");
            if (newTag != "") {
                this.tags.indexOf(newTag) === -1 ? this.tags.push(newTag) : "";
                input.value = "";
            }
        }
    }

    removeTagClick(tag: string)
    {
        this.tags = this.tags.filter((value) => (value != tag) ? value : "");
    }
    // ===
}
