import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ApiResult } from 'src/app/core/models/ApiResult';
import { Comment } from 'src/app/core/models/Comment';
import { CommentsService } from 'src/app/core/services/comments.service';

@Component({
	selector: 'app-comments-list',
	templateUrl: './comments-list.component.html'
})
export class CommentsListComponent implements OnInit {

	@Input()
	tutorialid: number;

    @Output()
	onCommentsSizeChange = new EventEmitter<number>();

	comments: Comment[];
	page: number = 1;
	pageSize: number = 10;
	commentsSize: number = 10;

	constructor(
		private commentsService: CommentsService
	) { }

	ngOnInit(): void {
		this.checkCommentsSize();
		this.loadComments();
	}

	public onPageChange(): void 
    {
        this.comments = null;
        this.loadComments();
    }

    public afterAddComment(): void
    {
        this.checkCommentsSize();
        this.page = 1;
        this.onPageChange();
    }

	private loadComments(): void
    {
        this.commentsService.getByTutorial(this.tutorialid, this.page, this.pageSize).subscribe(
            (comments: Comment[]) => {
                this.comments = comments;
            }
        )
    }

    private checkCommentsSize(): void
    {
        this.commentsService.countByTutorial(this.tutorialid).subscribe(
            (result: ApiResult) => {
                if(result.status == 1) {
                    this.commentsSize = result.data.cnt;
                    this.onCommentsSizeChange.emit(result.data.cnt);
                }
            }
        )
    }

}
