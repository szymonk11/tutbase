import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CategoriesComponent } from './categories/categories.component';
import { CategoryIndexComponent } from './categories/category-index/category-index.component';
import { CategoryResolver } from './categories/category-resolver.service';
import { HomeComponent } from './home/home.component';

import { LoginComponent } from './login/login.component';
import { PageNotFoundComponent } from './shared/pages/page-not-found/page-not-found.component';
import { TutorialAddComponent } from './tutorials/tutorial-add/tutorial-add.component';
import { TutorialIndexComponent } from './tutorials/tutorial-index/tutorial-index.component';
import { SearchByTagComponent } from './search/search-by-tag/search-by-tag.component';
import { RegisterComponent } from './register/register.component';
import { TutorialsFavoriteComponent } from './tutorials/tutorials-favorite/tutorials-favorite.component';
import { RegisterConfirmComponent } from './register/register-confirm/register-confirm.component';
import { PageContactComponent } from './shared/pages/page-contact/page-contact.component';
import { SearchComponent } from './search/search.component';
import { UserProfileComponent } from './user/user-profile/user-profile.component';
import { AdminIndexComponent } from './admin/admin-index/admin-index.component';
import { AdminResolver } from './admin/admin-resolver.service';
import { ResetPasswordComponent } from './user/reset-password/reset-password.component';
import { RemindPasswordComponent } from './user/remind-password/remind-password.component';
import { TutorialEditComponent } from './tutorials/tutorial-edit/tutorial-edit.component';
import { TutorialEditResolver } from './tutorials/tutorial-edit/tutorial-edit-resolver.service';
import { TutorialsListComponent } from './tutorials/tutorials-list/tutorials-list.component';
import { UserPreviewComponent } from './user/user-preview/user-preview.component';

const routes: Routes = [
	{ path: '', component: HomeComponent },
	{ path: 'login', component: LoginComponent, data: { title: 'Zaloguj się - Logowanie' } },
	{ path: 'register', component: RegisterComponent, data: { title: 'Załóż konto - Rejestracja' } },
	{ path: 'register/confirm/:userid/:register_key', component: RegisterConfirmComponent, data: { title: 'Potwierdź konto - Rejestracja' } },
	{ path: 'categories/:id', component: CategoryIndexComponent, resolve: { category: CategoryResolver } },
	{ path: 'categories', component: CategoriesComponent, data: { title: 'Wszystkie kategorie' } },
	{ path: 'tutorials/add', component: TutorialAddComponent, data: { title: 'Dodaj poradnik' } },
	{ path: 'tutorials/edit/:id', component: TutorialEditComponent, resolve: { iscanedit: TutorialEditResolver }, data: { title: 'Edytuj poradnik' } },
	{ path: 'tutorials/search', component: SearchComponent, data: { title: 'Wyszukiwanie' } },
	{ path: 'tutorials/tag/:tagname', component: SearchByTagComponent },
	{ path: 'tutorials/all', component: TutorialsListComponent, data: { title: 'Wszystkie poradniki' } },
	{ path: 'tutorials/:id', component: TutorialIndexComponent },
	{ path: 'favorite', component: TutorialsFavoriteComponent, data: { title: 'Ulubione' } },
	{ path: 'user/preview/:userid', component: UserPreviewComponent, data: { title: 'Użytkownik' } },
	{ path: 'user/profile', component: UserProfileComponent, data: { title: 'Twój profil' } },
	{ path: 'user/remind-password', component: RemindPasswordComponent, data: { title: 'Przypomnij hasło' } },
	{ path: 'user/reset-password/:userid/:reset_key', component: ResetPasswordComponent, data: { title: 'Ustaw nowe hasło' } },
	{ path: 'contact', component: PageContactComponent, data: { title: 'Kontakt' } },
	{ path: 'admin', component: AdminIndexComponent, resolve: { isadmin: AdminResolver }, data: { title: 'Panel administracyjny' } },
	{ path: '404/:error', component: PageNotFoundComponent, data: { title: 'Nie odnaleziono strony' } },
	{ path: '**', component: PageNotFoundComponent }
];

@NgModule({
	imports: [RouterModule.forRoot(routes, {
		onSameUrlNavigation: 'reload'
	})],
	exports: [RouterModule]
})
export class AppRoutingModule { }
