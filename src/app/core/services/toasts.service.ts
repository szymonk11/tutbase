import { Injectable, TemplateRef } from '@angular/core';
import { ApiResult } from '../models/ApiResult';

@Injectable({
	providedIn: 'root'
})
export class ToastsService {
	toasts: any[] = [];

	show(textOrTpl: string | TemplateRef<any>, options: any = {}) {
		this.toasts.push({ textOrTpl, ...options });
	}

	remove(toast) {
		this.toasts = this.toasts.filter(t => t !== toast);
	}

	success(textOrTpl: string | TemplateRef<any>, options: any = {}) {
		options.classname = 'bg-success text-light';
		this.toasts.push({ textOrTpl, ...options });
	}

	error(textOrTpl: string | TemplateRef<any>, options: any = {}) {
		options.classname = 'bg-danger text-light';
		this.toasts.push({ textOrTpl, ...options });
	}

	warning(textOrTpl: string | TemplateRef<any>, options: any = {}) {
		options.classname = 'bg-warning text-dark';
		this.toasts.push({ textOrTpl, ...options });
	}

	showResult(result: ApiResult): void {
		if(result.status == 1) {
			this.success(result.message);
		} else {
			this.error(result.message);
		}
	}
}
