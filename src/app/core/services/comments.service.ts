import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiResult } from '../models/ApiResult';
import { Comment } from '../models/Comment';
import { NewComment } from '../models/NewComment';
import { ApiService } from './api.service';

@Injectable({
	providedIn: 'root'
})
export class CommentsService {

	constructor(
		private apiService: ApiService
	) { }

	getByTutorial(tutorial: number, page: number = 1, pageSize: number = 10): Observable<Comment[]> {
        return this.apiService.get('/comments/tutorial/' + tutorial + '/' + page + '?forPage=' + pageSize);
    }

	countByTutorial(tutorial: number): Observable<ApiResult> {
        return this.apiService.get('/comments/tutorial/' + tutorial + '/count');
    }

	getLast(cnt: number = 5): Observable<Comment[]> {
        return this.apiService.get('/comments/last/' + cnt);
    }

	add(newComment: FormData): Observable<ApiResult> {
        return this.apiService.post('/comments/add', newComment);
    }
}
