import { HttpEvent, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Params } from '@angular/router';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { ApiResult } from '../models/ApiResult';
import { NewTutorial } from '../models/NewTutorial';
import { SearchResult } from '../models/SearchResult';
import { Tag } from '../models/Tag';
import { Tutorial } from '../models/Tutorial';
import { UpdateTutorial } from '../models/UpdateTutorial';
import { ApiService } from './api.service';

@Injectable({
    providedIn: 'root'
})
export class TutorialsService {

    constructor(
        private apiService: ApiService
    ) { }

    get(id: number): Observable<Tutorial> {
        return this.apiService.get('/tutorials/' + id);
    }

    getAll(page: number = 1, pageSize: number = 10, sort: string = null): Observable<Tutorial[]> {
        return this.apiService.get('/tutorials/all/' + page + '?forPage=' + pageSize + '&sort=' + sort);
    }

    countAll(): Observable<ApiResult> {
        return this.apiService.get('/tutorials/all/count');
    }

    getByCategory(category: number, page: number = 1, pageSize: number = 10, sort: string = null): Observable<Tutorial[]> {
        return this.apiService.get('/tutorials/bycategory/' + category + '/' + page + '?forPage=' + pageSize + '&sort=' + sort);
    }

    countByCategory(category: number): Observable<ApiResult> {
        return this.apiService.get('/tutorials/bycategory/' + category + '/count');
    };

    getByTag(tag: string, page: number = 1, pageSize: number = 10): Observable<Tutorial[]> {
        return this.apiService.get('/tutorials/bytag/' + tag + '/' + page + '?forPage=' + pageSize);
    }

    countByTag(tag: string): Observable<ApiResult> {
        return this.apiService.get('/tutorials/bytag/' + tag + '/count');
    };

    getByUser(userid: number): Observable<ApiResult> {
        return this.apiService.get('/tutorials/byuser/' + userid);
    }

    getLast(cnt: number = 5): Observable<Tutorial[]> {
        return this.apiService.get('/tutorials/last/' + cnt);
    }

    getTopViews(cnt: number = 5): Observable<Tutorial[]> {
        return this.apiService.get('/tutorials/topviews/' + cnt);
    }

    insertView(id: number): Observable<ApiResult> {
        return this.apiService.put('/tutorials/insertView/' + id);
    }

    add(newTutorial: NewTutorial): Observable<ApiResult> {
        return this.apiService.post('/tutorials/add', newTutorial);
        /*.pipe(tap((response) => {
            if(response.status == 1 && response.data.token) {
                this.tokenService.saveToken(response.data.token);
                localStorage.setItem("tutBaseUser", JSON.stringify(response.data.user));
                this._loggedInUser$.next(response.data.user);
            }
        }));*/
    }

    update(updateTutorial: UpdateTutorial): Observable<ApiResult> {
        return this.apiService.put('/tutorials/' + updateTutorial.id, updateTutorial);
    }

    uploadVideo(file: File): Observable<HttpEvent<any>> {
        let formData = new FormData();
        formData.append('videoFile', file, file.name);
        console.log(file);
        return this.apiService.upload('/tutorials/upload', formData);
    }

    afterUploaded(tutorialid: number, filename: string): Observable<ApiResult> {
        return this.apiService.post('/tutorials/savevideo', {
            tutorialid, filename
        });
    }

    search(params: Params, page: number = 1, pageSize: number = 10): Observable<SearchResult> {
        let httpParams = new HttpParams({
            fromObject: {...params, forPage: pageSize.toString()}
        });
        
        return this.apiService.get('/tutorials/search/' + page, httpParams );
    }

    isCanEdit(tutorialid: number): Observable<ApiResult> {
        return this.apiService.post('/tutorials/iscanedit', {
            tutorialid
        });
    }

    // favorites
    getFavorites(userid: number): Observable<Tutorial[]> {
        return this.apiService.get('/tutorials/favorite/' + userid)
        .pipe(tap((response) => {
            localStorage.setItem("tutBaseFavorites", JSON.stringify(response));
        }));
    }

    getFavoritesByUser(userid: number, page: number = 1, pageSize: number = 10): Observable<Tutorial[]> {
        return this.apiService.get('/tutorials/favorite/' + userid + '/' + page + '?forPage=' + pageSize);
    }

    countFavoritesByUser(userid: number): Observable<ApiResult> {
        return this.apiService.get('/tutorials/favorite/' + userid + '/count');
    };

    addFavorite(userid: number, tutorialid: number): Observable<ApiResult> {
        return this.apiService.post('/tutorials/favorite/', { userid, tutorialid })
        .pipe(tap((response) => {
            if(response.status == 1) {
                let favorites = [];
                if(localStorage['tutBaseFavorites']) {
                    favorites = JSON.parse(localStorage.getItem('tutBaseFavorites'));
                }
                if(favorites.indexOf(tutorialid) == -1) {
                    favorites.push(tutorialid);
                    localStorage.setItem("tutBaseFavorites", JSON.stringify(favorites));
                }
            }
        }));
    }

    deleteFavorite(userid: number, tutorialid: number): Observable<ApiResult> {
        return this.apiService.delete('/tutorials/favorite/' + userid + '/' + tutorialid)
        .pipe(tap((response) => {
            if(response.status == 1) {
                let favorites = [];
                if(localStorage['tutBaseFavorites']) {
                    favorites = JSON.parse(localStorage.getItem('tutBaseFavorites'));
                }
                let find = favorites.indexOf(tutorialid);
                if(find != -1) {
                    favorites.splice(find, 1);
                    localStorage.setItem("tutBaseFavorites", JSON.stringify(favorites));
                }
            }
        }));
    }

    loadFavorites(userid: number): void {
        this.getFavorites(userid).subscribe(
            (tutorials: Tutorial[]) => {
                let favorites = [];
                tutorials.forEach((tut) => favorites.push(tut.id));
                localStorage.setItem("tutBaseFavorites", JSON.stringify(favorites));
            }
        )
    }

    isFavorite(tutorialid: number): boolean {
        if(localStorage['tutBaseFavorites']) {
            let favorites = JSON.parse(localStorage.getItem('tutBaseFavorites'));
            return favorites.indexOf(tutorialid) != -1;
        } else {
            return false;
        }
    }

    // tags
    getTopTags(cnt: number = 10): Observable<Tag[]> {
        return this.apiService.get('/tags/top/' + cnt);
    }
}
