import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { ApiService } from './api.service';
import { TokenService } from './token.service';
import { LoggedInUser } from '../models/LoggedInUser';
import { ApiResult } from '../models/ApiResult';
import { TutorialsService } from './tutorials.service';
import { UserPreview } from '../models/UserPreview';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    private _loggedInUser$: BehaviorSubject<LoggedInUser>;
    public loggedInUser$: Observable<LoggedInUser>;

    constructor(
        private apiService: ApiService,
        private tokenService: TokenService,
        private tutorialsService: TutorialsService
    ) { 
        let user = this.getLocalUser();
        this._loggedInUser$ = new BehaviorSubject<LoggedInUser>(user);
        this.loggedInUser$ = this._loggedInUser$.asObservable();
        this.isLoggedIn().subscribe();
    }

    getLocalUser(): LoggedInUser {
        if(localStorage['tutBaseUser']) {
            return JSON.parse(localStorage.getItem('tutBaseUser'));
        } else {
            return null;
        }
    }

    isLoggedIn(): Observable<ApiResult> {
        return this.apiService.post('/user/isLoggedIn')
        .pipe(tap((response) => {
            if(response.status == 0) {
                this.tokenService.destroyToken();
                localStorage.removeItem("tutBaseUser");
                this._loggedInUser$.next(null);
            } 
            else if(response.status == 1) {
                if(response.data.token) {
                    this.tokenService.saveToken(response.data.token);
                }
            }
        }));
    }

    isAdmin(): Observable<ApiResult> {
        return this.apiService.post('/user/isAdmin');
    }

    login(email: string, password: string, remember: number = 0): Observable<ApiResult> {
        return this.apiService.post('/user/login', {
            email, password, remember
        })
        .pipe(tap((response) => {
            if(response.status == 1 && response.data.token) {
                this.tokenService.saveToken(response.data.token);
                localStorage.setItem("tutBaseUser", JSON.stringify(response.data.user));
                this._loggedInUser$.next(response.data.user);
                this.tutorialsService.loadFavorites(response.data.user.id);

                if(response.data.remember_token) {
                    this.tokenService.saveRememberToken(response.data.remember_token);
                }
            }
        }));
    }

    logout() {
        let token = this.tokenService.getToken();
        this.tokenService.destroyToken();
        localStorage.removeItem("tutBaseUser");
        this._loggedInUser$.next(null);

        this.apiService.post('/user/logout', {token}).subscribe();
    }

    register(email: string, password: string, name: string): Observable<ApiResult> {
        return this.apiService.post('/user/register', {
            email, password, name
        });
    }

    confirm(userid: number, register_key: string): Observable<ApiResult> {
        return this.apiService.post('/user/confirm', {
            userid, register_key
        });
    }

    changePassword(userid: number, password: string, newPassword: string): Observable<ApiResult> {
        return this.apiService.post('/user/changepassword', {
            userid, password, newpassword: newPassword
        });
    }

    remindPassword(email: string): Observable<ApiResult> {
        return this.apiService.post('/user/remindpassword', {
            email
        });
    }

    resetPassword(userid: number, reset_key: string, newPassword: string): Observable<ApiResult> {
        return this.apiService.post('/user/resetpassword', {
            userid, reset_key, newPassword
        });
    }

    // PREVIEW
    getUserPreview(userid: number): Observable<UserPreview> {
        return this.apiService.get('/user/preview/' + userid);
    }

    // STATISTICS
    getStatistics(): Observable<ApiResult> {
        return this.apiService.get('/utility/statistics');
    }
}
