import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Injectable({
	providedIn: 'root'
})
export class TitleService {

	suffix = "TutBase";

	constructor(
		private titleService: Title
	) { }

	setTitle(title: string): void {
		if(!title || title == "") {
			this.titleService.setTitle(this.suffix);
		} else {
			this.titleService.setTitle(title + " | " + this.suffix);
		}
	}
}
