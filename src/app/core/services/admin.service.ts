import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { ApiResult } from '../models/ApiResult';
import { Tutorial, TutorialStatus } from '../models/Tutorial';
import { ApiService } from './api.service';
import { CategoriesService } from './categories.service';

@Injectable({
	providedIn: 'root'
})
export class AdminService {

	constructor(
		private apiService: ApiService,
		private categoriesService: CategoriesService
	) { }

	getTutorialsByStatus(status: TutorialStatus): Observable<Tutorial[]> {
		return this.apiService.get('/admin/tutorials/bystatus/' + status);
	}

	setStatus(tutorialid: number, status: TutorialStatus, reason: string = null): Observable<ApiResult> {
		return this.apiService.put('/admin/tutorials/' + tutorialid + '/status', { status, reason });
	}

	deleteComment(commentid: number): Observable<ApiResult> {
		return this.apiService.delete('/admin/comments/' + commentid);
	}

	addCategory(name: string, parent_category: number = 0): Observable<ApiResult> {
		return this.apiService.post('/admin/categories', { name, parent_category })
			.pipe(tap((response) => {
				this.categoriesService.fetchAll().subscribe();
			}));
	}

	updateCategory(categoryid: number, name: string, parent_category: number = 0): Observable<ApiResult> {
		return this.apiService.put('/admin/categories/' + categoryid, { name, parent_category })
		.pipe(tap((response) => {
			this.categoriesService.fetchAll().subscribe();
		}));
	}

	deleteCategory(categoryid: number): Observable<ApiResult> {
		return this.apiService.delete('/admin/categories/' + categoryid)
		.pipe(tap((response) => {
			this.categoriesService.fetchAll().subscribe();
		}));
	}
}
