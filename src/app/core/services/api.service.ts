import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';

import { TokenService } from './token.service';

@Injectable({
    providedIn: 'root'
})
export class ApiService {
    constructor(
        private http: HttpClient,
        private tokenService: TokenService
    ) { }

    private prepareHeaders() {
        let headers = {};
        let token = this.tokenService.getToken();
        if(token) {
            headers["Authorization"] = token;
            //"Remember-Token": this.tokenService.getRememberToken()
        }
        let rememberToken = this.tokenService.getRememberToken();
        if(rememberToken) {
            headers["Remember-Token"] = rememberToken;
        }
        
        return new HttpHeaders(headers);
    }

    get(path: string, params: HttpParams = new HttpParams()): Observable<any> {
        return this.http.get(`${environment.api_url}${path}`, { params: params, headers: this.prepareHeaders() });
    }

    put(path: string, body: Object = {}, params: HttpParams = new HttpParams()): Observable<any> {
        return this.http.put(
            `${environment.api_url}${path}`,
            body, { params: params, headers: this.prepareHeaders() }
        );
    }

    post(path: string, body: Object = {}, params: HttpParams = new HttpParams()): Observable<any> {
        return this.http.post(
            `${environment.api_url}${path}`,
            body, { params: params, headers: this.prepareHeaders() }
        );
    }

    delete(path: string, params: HttpParams = new HttpParams()): Observable<any> {
        return this.http.delete(
            `${environment.api_url}${path}`, { params: params, headers: this.prepareHeaders() }
        );
    }

    upload(path: string, formData: FormData): Observable<any> {
        return this.http.post(
            `${environment.api_url}${path}`,
            formData, {
                headers: this.prepareHeaders(),
                reportProgress: true,
                observe: 'events'
            }
        );
    }
}
