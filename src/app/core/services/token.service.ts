import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class TokenService {

  getToken(): string {
    return localStorage['tutBaseToken'];
  }

  saveToken(token: string):void {
    localStorage['tutBaseToken'] = token;
  }

  destroyToken(): void {
    localStorage.removeItem('tutBaseToken');
    this.destroyRememberToken();
  }


  getRememberToken(): string {
    return localStorage['tutBaseRememberToken'];
  }

  saveRememberToken(token: string): void {
    localStorage['tutBaseRememberToken'] = token;
  }

  destroyRememberToken(): void {
    localStorage.removeItem('tutBaseRememberToken');
  }

}
