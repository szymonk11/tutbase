import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { ApiService } from './api.service';
import { Category } from '../models/Category';
import { ExtraField } from '../models/ExtraField';
import { tap } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class CategoriesService {

    private _categories$: BehaviorSubject<Category[]>;
    public categories$: Observable<Category[]>;

    constructor(
        private apiService: ApiService
    ) {
        this._categories$ = new BehaviorSubject<Category[]>([]);
        this.categories$ = this._categories$.asObservable();

        this.apiService.get('/categories/').subscribe(
            (categories: Category[]) => {
                this._categories$.next(categories);
            }
        )
    }

    get(id: number): Observable<Category> {
        return this.apiService.get('/categories/' + id);
    }

    getAll(): Observable<Category[]> {
        return this.categories$;
    }

    fetchAll(): Observable<Category[]> {
        return this.apiService.get('/categories/')
            .pipe(
                tap((categories: Category[]) => {
                    this._categories$.next(categories);
                })
            );
    }

    getSubCategories(parent_id: number): Observable<Category[]> {
        return this.apiService.get('/categories/subcategories/' + parent_id);
    }

    // EXTRA FIELDS
    getExtraFieldsByCategory(categoryid: number): Observable<ExtraField[]> {
        return this.apiService.get('/categories/extrafields/' + categoryid);
    }
}
