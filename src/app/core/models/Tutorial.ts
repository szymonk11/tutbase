import { Category } from "./Category";

export interface Tutorial 
{
    readonly id: number;
	readonly userid: number;
	readonly title: string;
	readonly description?: string;
	readonly content?: string;
	readonly url?: string;
	readonly filename?: string;
	readonly type: number;
    readonly categoryid: number;
	readonly created: string;
	readonly updated?: string;
	readonly difficulty: number;
	readonly status: TutorialStatus;
	readonly views: number;
	readonly visible: number;
	readonly closed: number;
	readonly lang: string;
	readonly extra_fields?: string;
	readonly reason?: string;

	readonly tags?: string;
	readonly category_name?: string;
    readonly category?: Category;
	readonly avg_usefullness?: number;
	readonly avg_difficulty?: number;
	readonly user_username?: string;
}

export enum TutorialType {
	Video = 1,
	Text,
	StepByStep
}

export enum TutorialStatus {
	Draft = 1,
	Pending,
	Verified,
	Rejected,
	ForImprovement
}