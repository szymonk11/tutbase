import { Category } from "./Category";

export interface UpdateTutorial 
{
	id: number;
	title: string;
	description?: string;
	content?: string;
	url?: string;
	type: number;
    categoryid: number;
	difficulty: number;
	lang: string;
	extra_fields?: string;

	tags?: string[];
}