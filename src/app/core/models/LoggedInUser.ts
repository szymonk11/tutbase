export interface LoggedInUser 
{
    readonly id: number;
	readonly username: string;
	readonly email: string;
	readonly regdate: Date;
	readonly isadmin: boolean;
}
