import { Category } from "./Category";

export interface NewTutorial 
{
	userid: number;
	title: string;
	description?: string;
	content?: string;
	url?: string;
	type: number;
    categoryid: number;
	difficulty: number;
	closed: number;
	lang: string;
	extra_fields?: string;

	tags?: string[];
}