export interface UserPreview 
{
    readonly id: number;
	readonly username: string;
	readonly regdate: Date;
}
