export interface NewComment 
{
    tutorialid: number;
	userid: number;
    usefulness: number;
    difficulty: number;
	content?: string;
}