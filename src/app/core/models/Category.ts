export interface Category 
{
    readonly id: number;
	readonly name: string;
	readonly description: string;
	readonly parent_category?: number;
	readonly childrens?: Category[];
}