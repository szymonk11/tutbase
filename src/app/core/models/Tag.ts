export interface Tag 
{
    readonly id: number;
    readonly name: string;
	readonly tutorials_cnt: number;
}