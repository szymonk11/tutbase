import { Tutorial } from "./Tutorial";

export interface SearchResult 
{
    readonly tutorials: Tutorial[];
	readonly total_cnt: number;
}