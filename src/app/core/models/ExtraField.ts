export interface ExtraField 
{
    readonly id: number;
    readonly categoryid: number;
	readonly name: string;
	readonly type: ExtraFieldType;
    readonly options: string[];
}

export enum ExtraFieldType {
    ShortText = 1,
    Options = 2
}
