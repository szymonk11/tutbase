export interface Comment 
{
    readonly id: number;
    readonly tutorialid: number;
	readonly userid: number;
	readonly created: string;
    readonly usefulness?: number;
    readonly difficulty?: number;
	readonly content?: string;
    readonly files?: string;

    readonly tutorial_title: string;
    readonly user_username?: string;
}