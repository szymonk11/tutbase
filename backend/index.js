const express = require('express');
const jwt = require('jsonwebtoken');
const cors = require('cors');

const CONFIG = require('./config');
const mysqlLib = require('./mysqlLib');
const userModule = require('./modules/user');
const categoriesModule = require('./modules/categories');
const tutorialsModule = require('./modules/tutorials');
const commentsModule = require('./modules/comments');
const tagsModule = require('./modules/tags');
const adminModule = require('./modules/admin');
const utilityModule = require('./modules/utility');

const app = express();

let utilityMethods = function (req, res, next) {
	res.sendError = (status, err = false, message = "Oops! Something went wrong!") => {
		if(err) {
			console.log(err);
		} else {
			console.log(message);
		}

		res.status(status).send({status, message});
	};

	res.sendResult = (status, message, data = {}, httpStatus = 200) => {
		res.status(httpStatus).send({status, message, data});
	};

	req.authorizeToken = (token_param = false) => {

		// check remember
		let checkRemember = () => {
			let remember_token = req.get('Remember-Token');
			if(remember_token) {
				let remember_decoded = jwt.verify(remember_token, CONFIG.secretkey);
				if(remember_decoded.data.agent == agent && remember_decoded.data.ip == ip) {
					let now = parseInt(Date.now().toString().substr(0, remember_decoded.exp.toString().length));
					if(remember_decoded.exp > now) {
						return { 
							id: remember_decoded.data.id, 
							username: remember_decoded.data.username, 
							email: remember_decoded.data.email,
							renew_token: true // nowy token w IsLoggedIn()
						};
					}
				}
			}

			return false;
		};

		let token = (token_param) ? token_param : req.get('Authorization');
		let agent = req.get('User-Agent');
		let ip = req.ip;

		if(!token || !agent || !ip)  {
			return false;
		}

		try {
			let decoded = jwt.verify(token, CONFIG.secretkey);
			if(decoded.data.agent == agent && decoded.data.ip == ip) {
			
				// check is not expired
				let now = parseInt(Date.now().toString().substr(0, decoded.exp.toString().length));
				if (decoded.exp < now) {

					return checkRemember();
				}
				// ====================
	
				return { id: decoded.data.id, username: decoded.data.username, email: decoded.data.email };
			}

			return false;
		}
		catch (error) {
			return checkRemember();
		}
	};

	req.isAdmin = (callback) => {
		let authorized = req.authorizeToken();
		if(!authorized) {
			callback(false);
		} else {
			mysqlLib.getConnection().query("SELECT admin FROM users WHERE id = ?", [ authorized.id ])
			.then((result) => {
				if(result && result.length > 0) {
					if(result[0].admin == 1) {
						callback(true);
						return;
					}
				}
	
				callback(false);
			});
		}
	}

	next();
};

let logger = function (req, res, next) {
	let d = new Date();
	console.log(d.toISOString() + " " + req.method + " " + req.originalUrl);
	next();
};

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({
	extended: true
}));
app.use(logger);
app.use(utilityMethods);
app.use('/files', express.static('uploads'));
app.use(function (err, req, res, next) {
	// Check if the error is thrown from multer
	if (err instanceof multer.MulterError) {
		res.statusCode = 400
		res.send({ code: err.code })
	}
})
app.use('/user', userModule);
app.use('/categories', categoriesModule);
app.use('/tutorials', tutorialsModule);
app.use('/comments', commentsModule);
app.use('/tags', tagsModule);
app.use('/admin', adminModule);
app.use('/utility', utilityModule);


app.get('/', (req, res) => {
	res.send('TutBase API. Copyright 2021-2022 by Szymon Karolczuk');
});

app.listen(CONFIG.port, CONFIG.host, () => {
	console.log(`TutBase API server listening at http://localhost:${CONFIG.port}`);
});
