const nodemailer = require('nodemailer');
const fs = require('fs');

const CONFIG = require('./config.js');

const FROM = '"TutBase" tutbase@szymonk.pl';

let transporter = nodemailer.createTransport({
   host: "mail.szymonk.pl",
   port: 465,
   secure: true,
   auth: {
       user: "tutbase@szymonk.pl",
       pass: "tR5vK3p4"
   } 
});

transporter.verify((error, success) => {
    if(error) {
        console.log("SMTP CONNECTION ERROR");
        console.log(error);
    } else {
        console.log("SMTP CONNECTION SUCCESS");
    }
});

exports.getTransporter = () => transporter;

exports.sendMail = (to, subject, text, html, callback) => {
    transporter.sendMail({
        from: FROM,
        to: to,
        subject: subject,
        text: text,
        html: html
    }, callback);
};

exports.sendRegisterMail = (to, userid, confirm_key, callback) => {
    fs.readFile('./emails/register.html', {
        encoding: 'utf8'
    }, (err, html) => {

        let confirm_url = CONFIG.get('url')+'register/confirm/'+userid+'/'+confirm_key;

        let htmlMessage = html.replace('${REGISTER_URL}', confirm_url);
        let text = 'Aby dokończyć rejestrację, potwierdź swój adres e-mail klikając w link obok - ' + confirm_url;
        this.sendMail(to, 'Rejestracja w serwisie TutBase', text, htmlMessage, callback);
    });
};


exports.sendResetPasswordMail = (to, userid, reset_key, callback) => {

    fs.readFile('./emails/reset_password.html', {
        encoding: 'utf8'
    }, (err, html) => {

        let reset_url = CONFIG.get('url')+'user/reset-password/'+userid+'/'+reset_key;

        let htmlMessage = html.replace('${RESET_URL}', reset_url);
        let text = 'Aby dokończyć proces i ustawić nowe hasło kliknij w link obok - ' + reset_url;
        this.sendMail(to, 'Reset hasła w serwisie TutBase', text, htmlMessage, callback);
    });

};
