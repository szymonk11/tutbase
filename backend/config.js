const fs = require('fs');

let CONFIG = null;

module.exports = {

    getConfig() {
        if(!CONFIG) {
            let raw = fs.readFileSync('./config.json');
            CONFIG = JSON.parse(raw);
        }

        return CONFIG;
    },

    set(key, value) {
        CONFIG[key] = value;
        fs.writeFileSync('./config.json', JSON.stringify(CONFIG, null, 4));
    },

    get(key) {
        if(!CONFIG) {
            let raw = fs.readFileSync('./config.json');
            CONFIG = JSON.parse(raw);
        }

        return CONFIG[key];
    },

    get port() {
        return this.get('port');
    },
    get host() {
        return this.get('host');
    },
    get secretkey() {
        let expired = Date.now()
        let timestamp = this.get('secretkey_timestamp') + 7776000000; // +90 dni
        if(timestamp < expired) {
            let secretkey = makeid(32);
            this.set('secretkey', secretkey);
            this.set('secretkey_timestamp', expired);
            return secretkey;
        }
        
        return this.get('secretkey');
    },
};

function makeid(length) {
    var result = [];
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result.push(characters.charAt(Math.floor(Math.random() *
            charactersLength)));
    }
    return result.join('');
}
