const express = require('express');
const fs = require('fs')
const multer = require('multer');
const mysqlLib = require('../mysqlLib');
const CONFIG = require('../config');

const router = express.Router();

const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        let uploadPath = __dirname + '/../' + CONFIG.get('upload_comments');
        if(fs.existsSync(uploadPath)) {
            cb(null, uploadPath);
        } else {
            console.log(uploadPath);
            cb(new Error("Ścieżka nie istnieje."), uploadPath);
        }
    },
    filename: function(req, file, cb) {
        cb(null, Date.now() + "_" + file.originalname);
    }
});
const uploader = multer({
    storage: storage,
    limits: {
        fileSize: 1024 * 1024 * 2 // 2MB
    },
    fileFilter: function(req, file, cb) {
        if(file.mimetype == "image/jpg" || file.mimetype == "image/jpeg" || file.mimetype == "image/png") {
            cb(null, true);
        } else {
            cb(new Error("Nie obsługiwany format pliku"), false);
        }
    }
});


router.get('/tutorial/:id/count', function(req, res) {

    mysqlLib.getConnection().query("SELECT COUNT(*) AS cnt from comments WHERE tutorialid = ? ORDER BY created DESC", [req.params.id])
    .then((comments) => {
        
        res.sendResult(1, "", { cnt: comments[0].cnt });
    })
    .catch((err) => {
        res.sendError(500, err);
    });

});

router.get('/tutorial/:id/:page', function(req, res) {

    let forPage = parseInt(req.query.forPage);
    if(!forPage) forPage = 10;
    let offset = (req.params.page - 1) * forPage;

    mysqlLib.getConnection().query("SELECT c.*, u.username as user_username FROM comments AS c LEFT JOIN users u ON c.userid = u.id WHERE c.tutorialid = ? ORDER BY created DESC LIMIT ?, ?", [req.params.id, offset, forPage])
    .then((comments) => {
        
        res.send(comments);
    })
    .catch((err) => {
        res.sendError(500, err);
    });

});

router.get('/last/:cnt', function(req, res) {

    mysqlLib.getConnection().query("SELECT c.*, u.username AS user_username, t.title AS tutorial_title FROM comments AS c LEFT JOIN users u ON c.userid = u.id LEFT JOIN tutorials t ON c.tutorialid = t.id ORDER BY c.created DESC LIMIT ?", [parseInt(req.params.cnt)])
    .then((comments) => {
        
        res.send(comments);
    })
    .catch((err) => {
        res.sendError(500, err);
    });

});

router.post('/add', uploader.array('photos[]', 5), function(req, res) {

    let files = [];
    if(req.files) {
        req.files.forEach((file) => {
            files.push(file.filename);    
        });
    }

    let files_value = (files.length > 0) ? JSON.stringify(files) : null;

    mysqlLib.getConnection().query("INSERT INTO comments (tutorialid, userid, usefulness, difficulty, content, files) VALUES(?, ?, ?, ?, ?, ?)", [
        req.body.tutorialid, req.body.userid, req.body.usefulness, req.body.difficulty, req.body.content, files_value
    ])
    .then((result) => {
        let commentid = result.insertId;

        res.sendResult(1, "Recenzja została pomyślnie dodana.", { commentid });
    })
    .catch((err) => {
        res.sendError(500, err);
    });

});


module.exports = router;