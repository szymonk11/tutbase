const express = require('express');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const mysqlLib = require('../mysqlLib');

const router = express.Router();

const JOIN_CATEGORY = "LEFT JOIN categories AS c ON c.id = t.categoryid";


let checkAdmin = function(req, res, next) {
    req.isAdmin((is) => {
        if(is === true) {
            next();
        } else {
            res.sendError(404);
        }
    });
};
router.use(checkAdmin);


router.get('/tutorials/bystatus/:status', function(req, res) {

    mysqlLib.getConnection().query("SELECT t.*, GROUP_CONCAT(tags.name SEPARATOR ',') AS tags, c.name AS category_name FROM tutorials AS t "+JOIN_CATEGORY+" LEFT JOIN tutorials_tags AS tt ON tt.tutorial = t.id LEFT JOIN tags ON tags.id = tt.tag WHERE t.status = ? GROUP BY t.id ORDER BY created DESC", [req.params.status])
    .then((tutorials) => {
        
        res.send(tutorials);
    })
    .catch((err) => {
        res.sendError(500, err);
    });

});

router.put('/tutorials/:id/status', function(req, res) {

    if(req.body.status && req.params.id) {

        let status = parseInt(req.body.status);
        let reason = req.body.reason ?? "";
        let visible = (status == 3) ? 1 : 0; // 1 for Verified status

        mysqlLib.getConnection().query("UPDATE tutorials SET status = ?, visible = ?, reason = NULLIF(?, '') WHERE id = ? LIMIT 1", [status, visible, reason, parseInt(req.params.id)])
        .then((result) => {
            res.sendResult(1, "Status został zmieniony.");
        })
        .catch((err) => {
            res.sendError(500, err);
        });
    } else {
        res.sendError(500, "Błędne dane...");
    }
});

router.delete('/comments/:id', function(req, res) {

    if(parseInt(req.params.id)) {
        
        mysqlLib.getConnection().query("DELETE FROM comments WHERE id = ?", [parseInt(req.params.id)])
        .then((result) => {
            res.sendResult(1, "Komentarz został pomyślnie usunięty.");
        })
        .catch((err) => {
            res.sendError(500, err);
        });
    } else {
        res.sendError(500, "Błędne dane...");
    }
});

router.post('/categories', function(req, res) {

    let name = req.body.name;
    let parent_category = req.body.parent_category ?? '';

    mysqlLib.getConnection().query("INSERT INTO categories (name, parent_category) VALUES (?, NULLIF(?, ''))", [name, parent_category])
    .then((result) => {
        res.sendResult(1, "Kategoria została dodana.");
    })
    .catch((err) => {
        res.sendError(500, err);
    });
});

router.put('/categories/:id', function(req, res) {

    if(parseInt(req.params.id)) {

        let name = req.body.name;
        let parent_category = req.body.parent_category ?? '';

        mysqlLib.getConnection().query("UPDATE categories SET name = ?, parent_category = NULLIF(?, '') WHERE id = ? LIMIT 1", [name, parent_category, parseInt(req.params.id)])
        .then((result) => {
            res.sendResult(1, "Kategoria została zaktualizowana.");
        })
        .catch((err) => {
            res.sendError(500, err);
        });
    } else {
        res.sendError(500, "Błędne dane...");
    }
});

router.delete('/categories/:id', function(req, res) {

    if(parseInt(req.params.id)) {
       
        let category_id = parseInt(req.params.id);

        mysqlLib.getConnection().query(`SELECT t.* FROM tutorials AS t WHERE (t.categoryid = ? OR t.categoryid IN (SELECT id FROM categories WHERE parent_category = ?))`, [category_id, category_id])
        .then((result) => {
            if(result && result.length > 0) {

                res.sendResult(0, "Nie można usunąć kategorii, ponieważ zawiera już jakieś materiały.");
                return;
            } else {

                mysqlLib.getConnection().query("DELETE FROM categories WHERE id = ?", [parseInt(req.params.id)])
                .then((result) => {
                    res.sendResult(1, "Kategoria została pomyślnie usunięta.");
                })
                .catch((err) => {
                    res.sendError(500, err);
                });
            }
        })
        .catch((err) => {
            res.sendError(500, err);
        });
    } else {
        res.sendError(500, "Błędne dane...");
    }
});


module.exports = router;