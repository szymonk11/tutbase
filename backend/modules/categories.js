const express = require('express');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const mysqlLib = require('../mysqlLib');

const router = express.Router();

router.get('/', function(req, res) {

    mysqlLib.getConnection().query("SELECT * from categories ORDER BY parent_category ASC, name ASC")
    .then((categories) => {

        let result = [];

        if(categories.length > 0) {

            categories.forEach(category => {
                if(!category.parent_category) {
                    result.push(category);
                } else {
                    let i = result.findIndex((v) => v.id == category.parent_category);
                    if(i != -1) {
                        if(!result[i].childrens) result[i].childrens = [];
                        result[i].childrens.push(category);
                    }
                }
            });

        }
            
        res.send(result);
    })
    .catch((err) => {
        res.sendError(500, err);
    });
});

router.get('/subcategories/:id', function(req, res) {

    mysqlLib.getConnection().query("SELECT * from categories WHERE parent_category = ? ORDER BY name", [req.params.id])
    .then((categories) => {
            
        res.send(categories);
    })
    .catch((err) => {
        res.sendError(500, err);
    });
});

router.get('/:id', function(req, res) {

    mysqlLib.getConnection().query("SELECT * FROM categories WHERE id = ? LIMIT 1", [parseInt(req.params.id)])
    .then((categories) => {
            
        if(!categories || categories.length < 1) {
            res.sendError(404, false, "Nie odnaleziono kategorii.");
            return;
        }

        res.send(categories.shift());
    })
    .catch((err) => {
        res.sendError(500, err);
    });
});

router.get('/extrafields/:category', function(req, res) {

    let categoryid = parseInt(req.params.category);

    mysqlLib.getConnection().query("SELECT * FROM extra_fields WHERE categoryid = ? OR categoryid = (SELECT parent_category FROM categories WHERE id = ?) ORDER BY name", [categoryid, categoryid])
    .then((extrafields) => {
            
        extrafields.map((value, index) => {
            extrafields[index].options = JSON.parse(value.options);
            if(extrafields[index].options) 
                extrafields[index].options.sort();
        });

        res.send(extrafields);
    })
    .catch((err) => {
        res.sendError(500, err);
    });

});


module.exports = router;