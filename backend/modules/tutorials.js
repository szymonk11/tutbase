const express = require('express');
const fs = require('fs')
const multer = require('multer');
const mysqlLib = require('../mysqlLib');
const CONFIG = require('../config');

const router = express.Router();

const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        let uploadPath = __dirname + '/../' + CONFIG.get('upload_tutorials');
        if(fs.existsSync(uploadPath)) {
            cb(null, uploadPath);
        } else {
            console.log(uploadPath);
            cb(new Error("Ścieżka nie istnieje."), uploadPath);
        }
    },
    filename: function(req, file, cb) {
        cb(null, Date.now() + "_" + file.originalname);
    }
});
const uploader = multer({
    storage: storage,
    limits: {
        fileSize: 1024 * 1024 * 50 // 50MB
    },
    fileFilter: function(req, file, cb) {
        if(file.mimetype == "video/mp4" || file.mimetype == "video/mkv") {
            cb(null, true);
        } else {
            cb(new Error("Nie obsługiwany format pliku"), false);
        }
    }
});

const JOIN_CATEGORY = "LEFT JOIN categories AS c ON c.id = t.categoryid";
const JOIN_USER = "LEFT JOIN users AS u ON u.id = t.userid";
const WHERE_VISIBLE_ACTIVE = "(t.visible = 1 AND t.status = 3)";

const getOrderByString = (sort) => {

    if(!sort) {
        return '';
    }

    let str = 'ORDER BY ';

    switch(sort) 
    {
        case 'added':
            str += 'created DESC';
            break;
        case 'views':
            str += ' views DESC';
            break;
        case 'comments':
            str += ' comments_cnt DESC';
            break;
        default:
            return '';
    }

    return str;
};

router.get('/all/count', function(req, res) {

    mysqlLib.getConnection().query(`SELECT COUNT(*) AS cnt from tutorials t WHERE ${WHERE_VISIBLE_ACTIVE}`)
    .then((tutorials) => {
        
        res.sendResult(1, "", { cnt: tutorials[0].cnt });
    })
    .catch((err) => {
        res.sendError(500, err);
    });

});

router.get('/all/:page', function(req, res) {

    let forPage = parseInt(req.query.forPage);
    if(!forPage) forPage = 10;
    let offset = (req.params.page - 1) * forPage;

    let sort = req.query.sort;
    if(sort == 'null' || !sort) sort = 'added';

    mysqlLib.getConnection().query(`SELECT t.*, GROUP_CONCAT(tags.name SEPARATOR ',') AS tags, c.name AS category_name, (SELECT COUNT(*) FROM comments WHERE tutorialid = t.id) AS comments_cnt FROM tutorials AS t ${JOIN_CATEGORY} LEFT JOIN tutorials_tags AS tt ON tt.tutorial = t.id LEFT JOIN tags ON tags.id = tt.tag WHERE ${WHERE_VISIBLE_ACTIVE} GROUP BY t.id `+ getOrderByString(sort) +` LIMIT ?, ?`, [offset, forPage])
    .then((tutorials) => {
        
        res.send(tutorials);
    })
    .catch((err) => {
        res.sendError(500, err);
    });

});

router.get('/bycategory/:id/count', function(req, res) {

    mysqlLib.getConnection().query(`SELECT COUNT(*) AS cnt from tutorials t WHERE ${WHERE_VISIBLE_ACTIVE} AND (categoryid = ? OR categoryid IN (SELECT id FROM categories WHERE parent_category = ?))`, [req.params.id, req.params.id])
    .then((tutorials) => {
        
        res.sendResult(1, "", { cnt: tutorials[0].cnt });
    })
    .catch((err) => {
        res.sendError(500, err);
    });

});

router.get('/bycategory/:id/:page', function(req, res) {

    let forPage = parseInt(req.query.forPage);
    if(!forPage) forPage = 10;
    let offset = (req.params.page - 1) * forPage;

    let sort = req.query.sort;
    if(sort == 'null' || !sort) sort = 'added';

    mysqlLib.getConnection().query(`SELECT t.*, GROUP_CONCAT(tags.name SEPARATOR ',') AS tags, c.name AS category_name, (SELECT COUNT(*) FROM comments WHERE tutorialid = t.id) AS comments_cnt FROM tutorials AS t ${JOIN_CATEGORY} LEFT JOIN tutorials_tags AS tt ON tt.tutorial = t.id LEFT JOIN tags ON tags.id = tt.tag WHERE ${WHERE_VISIBLE_ACTIVE} AND (t.categoryid = ? OR t.categoryid IN (SELECT id FROM categories WHERE parent_category = ?)) GROUP BY t.id `+ getOrderByString(sort) +` LIMIT ?, ?`, [req.params.id, req.params.id, offset, forPage])
    .then((tutorials) => {
        
        res.send(tutorials);
    })
    .catch((err) => {
        res.sendError(500, err);
    });

});

router.get('/bycategory/:id', function(req, res) {

    mysqlLib.getConnection().query(`SELECT t.*, GROUP_CONCAT(tags.name SEPARATOR ',') AS tags, c.name AS category_name FROM tutorials AS t ${JOIN_CATEGORY} LEFT JOIN tutorials_tags AS tt ON tt.tutorial = t.id LEFT JOIN tags ON tags.id = tt.tag WHERE ${WHERE_VISIBLE_ACTIVE} AND (t.categoryid = ? OR t.categoryid IN (SELECT id FROM categories WHERE parent_category = ?)) GROUP BY t.id ORDER BY created DESC`, [req.params.id, req.params.id])
    .then((tutorials) => {
        
        res.send(tutorials);
    })
    .catch((err) => {
        res.sendError(500, err);
    });

});

router.get('/last/:cnt', function(req, res) {

    mysqlLib.getConnection().query(`SELECT t.*, GROUP_CONCAT(tags.name SEPARATOR ',') AS tags, c.name AS category_name FROM tutorials AS t ${JOIN_CATEGORY} LEFT JOIN tutorials_tags AS tt ON tt.tutorial = t.id LEFT JOIN tags ON tags.id = tt.tag WHERE ${WHERE_VISIBLE_ACTIVE} GROUP BY t.id ORDER BY created DESC LIMIT ?`, [parseInt(req.params.cnt)])
    .then((tutorials) => {
        
        res.send(tutorials);
    })
    .catch((err) => {
        res.sendError(500, err);
    });

});

router.get('/bytag/:tag/count', function(req, res) {

    mysqlLib.getConnection().query(`SELECT COUNT(*) AS cnt FROM tutorials AS t LEFT JOIN tutorials_tags AS tt ON tt.tutorial = t.id LEFT JOIN tags ON tags.id = tt.tag WHERE ${WHERE_VISIBLE_ACTIVE} AND tags.name = ? GROUP BY t.id`, [req.params.tag])
    .then((tutorials) => {
        
        res.sendResult(1, "", { cnt: tutorials[0].cnt });
    })
    .catch((err) => {
        res.sendError(500, err);
    });

});

router.get('/bytag/:tag/:page', function(req, res) {

    let forPage = parseInt(req.query.forPage);
    if(!forPage) forPage = 10;
    let offset = (req.params.page - 1) * forPage;

    mysqlLib.getConnection().query(`SELECT t.*, GROUP_CONCAT(tags.name SEPARATOR ',') AS tags, c.name AS category_name FROM tutorials AS t ${JOIN_CATEGORY} LEFT JOIN tutorials_tags AS tt ON tt.tutorial = t.id LEFT JOIN tags ON tags.id = tt.tag WHERE ${WHERE_VISIBLE_ACTIVE} AND tags.name = ? GROUP BY t.id ORDER BY created DESC LIMIT ?, ?`, [req.params.tag, offset, forPage])
    .then((tutorials) => {
        
        res.send(tutorials);
    })
    .catch((err) => {
        res.sendError(500, err);
    });

});

router.get('/byuser/:userid', function(req, res) {

    let authorized = req.authorizeToken();
    if(authorized) {
        
        let isLoggedInUser = authorized.id == parseInt(req.params.userid);

        mysqlLib.getConnection().query(`SELECT t.*, GROUP_CONCAT(tags.name SEPARATOR ',') AS tags, c.name AS category_name FROM tutorials AS t ${JOIN_CATEGORY} LEFT JOIN tutorials_tags AS tt ON tt.tutorial = t.id LEFT JOIN tags ON tags.id = tt.tag WHERE (t.visible = 1 ` + (isLoggedInUser ? `OR t.status = 5` : ``) + `) AND t.userid = ? GROUP BY t.id ORDER BY created DESC`, [parseInt(req.params.userid)])
        .then((tutorials) => {
            
            res.sendResult(1, "", tutorials);
        })
        .catch((err) => {
            res.sendError(500, err);
        });

    } else {
        res.sendResult(0, "Twoja sesja wygasła. Zaloguj się ponownie!");
    }
});

router.get('/topviews/:cnt', function(req, res) {

    mysqlLib.getConnection().query(`SELECT t.*, GROUP_CONCAT(tags.name SEPARATOR ',') AS tags, c.name AS category_name FROM tutorials AS t ${JOIN_CATEGORY} LEFT JOIN tutorials_tags AS tt ON tt.tutorial = t.id LEFT JOIN tags ON tags.id = tt.tag WHERE ${WHERE_VISIBLE_ACTIVE} GROUP BY t.id ORDER BY t.views DESC LIMIT ?`, [parseInt(req.params.cnt)])
    .then((tutorials) => {
        
        res.send(tutorials);
    })
    .catch((err) => {
        res.sendError(500, err);
    });

});

router.put('/insertView/:id', function(req, res) {

    mysqlLib.getConnection().query("UPDATE tutorials SET views = views + 1 WHERE id = ? LIMIT 1", [parseInt(req.params.id)])
    .then((tutorials) => {
        res.sendResult(1, "Okay.");
    })
    .catch((err) => {
        res.sendError(500, err);
    });

});

router.get('/search/:page', function(req, res) {

    let forPage = parseInt(req.query.forPage);
    if(!forPage) forPage = 10;
    let offset = (req.params.page - 1) * forPage;

    let params = [];
    let where = WHERE_VISIBLE_ACTIVE;

    if(req.query.slug) {
        let like = '%' + req.query.slug + '%';
        where += " AND (t.title LIKE ? OR t.description LIKE ? OR t.content LIKE ?)";
        params.push(like, like, like);
    }

    if(req.query.categoryid) {
        where += " AND (t.categoryid = ? OR t.categoryid IN (SELECT id FROM categories WHERE parent_category = ?))";
        params.push(parseInt(req.query.categoryid), parseInt(req.query.categoryid));
    }

    if(req.query.type) {
        where += " AND t.type = ?";
        params.push(parseInt(req.query.type));
    }

    if(req.query.language) {
        where += " AND t.lang = ?";
        params.push(req.query.language);
    }

    //extras
    for(let prop in req.query) {
        if(prop.indexOf('extra_') != -1) {
            let split = prop.split('_');

            where += " AND t.extra_fields LIKE ?";
            params.push('%' + req.query[prop] + '%');
        }
    }

    mysqlLib.getConnection().query(`SELECT t.*, GROUP_CONCAT(tags.name SEPARATOR ',') AS tags, c.name AS category_name FROM tutorials AS t ${JOIN_CATEGORY} LEFT JOIN tutorials_tags AS tt ON tt.tutorial = t.id LEFT JOIN tags ON tags.id = tt.tag WHERE ${where} GROUP BY t.id ORDER BY created DESC`, params)
    .then((tutorials) => {
        
        if(!tutorials || tutorials.length < 1) {
            res.send({
                tutorials: [],
                totla_cnt: 0
            });
            return;
        }
        
        let result = tutorials.slice(offset, forPage * req.params.page);
        res.send({
            tutorials: result,
            total_cnt: tutorials.length
        });
    })
    .catch((err) => {
        res.sendError(500, err);
    });
});

router.get('/:id', function(req, res) {

    mysqlLib.getConnection().query(
        "SELECT t.*, GROUP_CONCAT(tags.name SEPARATOR ',') AS tags, c.name AS category_name, "
        + "(SELECT AVG(usefulness) FROM comments WHERE tutorialid = t.id) AS avg_usefullness, "
        + "(SELECT AVG(difficulty) FROM comments WHERE tutorialid = t.id) AS avg_difficulty, u.username AS user_username "
        + "FROM tutorials AS t "
        + "LEFT JOIN categories AS c ON c.id = t.categoryid "
        + "LEFT JOIN users AS u ON u.id = t.userid "
        + "LEFT JOIN tutorials_tags AS tt ON tt.tutorial = t.id "
        + "LEFT JOIN tags ON tags.id = tt.tag "
        + "WHERE t.id = ? GROUP BY t.id LIMIT 1", 
        [req.params.id])
    .then((tutorials) => {
        
        if(!tutorials || tutorials.length < 1) {
            res.sendError(404, false, "Nie odnaleziono poradnika.");
            return;
        }

        let tutorial = tutorials.shift()

        if(tutorial.visible == 1) {
            res.send(tutorial);
            return;

        } else {
            let user = req.authorizeToken();
            if(user && user.id == tutorial.userid) {
                res.send(tutorial);
                return;
            } else {
                req.isAdmin((is) => {
                    if(is === true) {
                        res.send(tutorial);
                        return;
                    } else {
                        res.sendError(404, false, "Nie odnaleziono poradnika.");
                        return;
                    }
                });
            }
        }
        
    })
    .catch((err) => {
        res.sendError(500, err);
        return;
    });
});


router.post('/add', function(req, res) {

    let conn = mysqlLib.getConnection();
    let tutorialid;

    conn.beginTransaction()
    .then(() => {
        
        let status = 2; // pending
        if(req.body.type == 1 && !req.body.url) {
            status = 1; // draft
        }

        conn.query("INSERT INTO tutorials (userid, title, description, content, url, type, categoryid, difficulty, lang, extra_fields, visible, status) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 0, ?)", [
            req.body.userid, req.body.title, req.body.description, req.body.content, req.body.url, req.body.type, req.body.categoryid, req.body.difficulty, req.body.lang, req.body.extra_fields, status
        ])
        .then((result) => {

            tutorialid = result.insertId;
            res.sendResult(1, "Poradnik został pomyślnie przesłany.", { tutorialid });

            // INSERT tags
            if(req.body.tags && req.body.tags.length > 0) {
                let tagsWhere = "";
                req.body.tags.forEach((t) => {
                    tagsWhere += tagsWhere == "" ? "'" + t + "'" : ",'" + t + "'";
                });
    
                conn.query("SELECT * FROM tags WHERE name IN (" + tagsWhere + ")")
                .then((tags) => {
                    req.body.tags.forEach((tag) => {

                        let find = tags.find(el => el.name == tag);
                        if(find) {

                            conn.query("INSERT INTO tutorials_tags (tag, tutorial) VALUES(?, ?)", [ find.id, tutorialid ]);

                        } else {

                            conn.query("INSERT INTO tags (name) VALUES(?)", [ tag ])
                            .then((result2) => {

                                if(result2) {
                                    conn.query("INSERT INTO tutorials_tags (tag, tutorial) VALUES(?, ?)", [ result2.insertId, tutorialid ]);
                                }
                            });
                        }
                    });
                });
            }
        });
        
    })
    .then(() => {
        conn.commit();
        //res.sendResult(1, "Poradnik został pomyślnie przesłany.", { tutorialid: tutorialid });
    })
    .catch((err) => {
        conn.rollback();
        res.sendError(500, err);
    });

});

router.post('/upload', uploader.single('videoFile'), function(req, res, next) {
    if(!req.file) {
        res.sendError(404, false, "Brak pliku.");
    } else {
        res.sendResult(1, "Plik został pomyślnie wysłany.", { filename: req.file.filename });
    }
});

router.post('/savevideo', function(req, res) {
    // status to pending
    mysqlLib.getConnection().query("UPDATE tutorials SET filename = ?, status = 2 WHERE id = ? LIMIT 1", [req.body.filename, parseInt(req.body.tutorialid)])
    .then((result) => {
        res.sendResult(1, "Okay.");
    })
    .catch((err) => {
        res.sendError(500, err);
    });
});

router.post('/iscanedit', function(req, res) {

    let authorized = req.authorizeToken();
    if(authorized) {

        req.isAdmin((is) => {
            if(is === true) {
                res.sendResult(1, "Okay.");
            } else {
                
                mysqlLib.getConnection().query("SELECT * FROM tutorials WHERE id = ? AND userid = ?", [req.body.tutorialid, authorized.id])
                .then((tutorials) => {
                    
                    if(tutorials.length > 0) {
                        res.sendResult(1, "Okay.");
                    } else {
                        res.sendResult(0, "Brak uprawnień.");
                    }

                })
                .catch((err) => {
                    res.sendError(500, err);
                });
            }
        });

    } else {
        res.sendResult(0, "Brak uprawnień.");
    }
});

router.put('/:id', function(req, res) {

    let authorized = req.authorizeToken();
    if(authorized) {

        let conn = mysqlLib.getConnection();

        conn.beginTransaction()
        .then(() => {
            
            let status = 2; // pending

            conn.query("UPDATE tutorials SET updated = CURRENT_TIMESTAMP(), status = ?, visible = 0, title = ?, description = ?, content = ?, url = ?, type = ?, categoryid = ?, difficulty = ?, lang = ?, extra_fields = ? WHERE id = ? AND userid = ?", [
                status, req.body.title, req.body.description, req.body.content, req.body.url, req.body.type, req.body.categoryid, req.body.difficulty, req.body.lang, req.body.extra_fields, req.body.id, authorized.id
            ])
            .then((result) => {

                res.sendResult(1, "Dany zostały pomyślnie zapisane.", req.body);

                // DELETE tags
                conn.query("DELETE FROM tutorials_tags WHERE tutorial = ?", [req.body.id]);

                // INSERT tags
                if(req.body.tags && req.body.tags.length > 0) {
                    let tagsWhere = "";
                    req.body.tags.forEach((t) => {
                        tagsWhere += tagsWhere == "" ? "'" + t + "'" : ",'" + t + "'";
                    });
        
                    conn.query("SELECT * FROM tags WHERE name IN (" + tagsWhere + ")")
                    .then((tags) => {
                        req.body.tags.forEach((tag) => {

                            let find = tags.find(el => el.name == tag);
                            if(find) {

                                conn.query("INSERT INTO tutorials_tags (tag, tutorial) VALUES(?, ?)", [ find.id, req.body.id ]);

                            } else {

                                conn.query("INSERT INTO tags (name) VALUES(?)", [ tag ])
                                .then((result2) => {

                                    if(result2) {
                                        conn.query("INSERT INTO tutorials_tags (tag, tutorial) VALUES(?, ?)", [ result2.insertId, req.body.id ]);
                                    }
                                });
                            }
                        });
                    });
                }
            });
            
        })
        .then(() => {
            conn.commit();
            //res.sendResult(1, "Poradnik został pomyślnie przesłany.", { tutorialid: tutorialid });
        })
        .catch((err) => {
            conn.rollback();
            res.sendError(500, err);
        });


    } else {
        res.sendResult(0, "Brak uprawnień.");
    }
});



// favoriteS
router.get('/favorite/:userid/count', function(req, res) {

    mysqlLib.getConnection().query(`SELECT COUNT(*) AS cnt FROM favorites WHERE ${WHERE_VISIBLE_ACTIVE} AND userid = ?`, [req.params.userid])
    .then((tutorials) => {
        
        res.sendResult(1, "", { cnt: tutorials[0].cnt });
    })
    .catch((err) => {
        res.sendError(500, err);
    });

});

router.get('/favorite/:userid/:page', function(req, res) {

    let forPage = parseInt(req.query.forPage);
    if(!forPage) forPage = 10;
    let offset = (req.params.page - 1) * forPage;

    mysqlLib.getConnection().query(`SELECT t.*, GROUP_CONCAT(tags.name SEPARATOR ',') AS tags, c.name AS category_name FROM favorites AS fav INNER JOIN tutorials AS t ON t.id = fav.tutorialid ${JOIN_CATEGORY} LEFT JOIN tutorials_tags AS tt ON tt.tutorial = t.id LEFT JOIN tags ON tags.id = tt.tag WHERE ${WHERE_VISIBLE_ACTIVE} AND fav.userid = ? GROUP BY t.id ORDER BY created DESC LIMIT ?, ?`, [req.params.userid, offset, forPage])
    .then((tutorials) => {
        
        res.send(tutorials);
    })
    .catch((err) => {
        res.sendError(500, err);
    });

});

router.get('/favorite/:userid', function(req, res) {

    mysqlLib.getConnection().query(`SELECT t.*, GROUP_CONCAT(tags.name SEPARATOR ',') AS tags, c.name AS category_name FROM favorites AS f INNER JOIN tutorials AS t ON f.tutorialid = t.id ${JOIN_CATEGORY} LEFT JOIN tutorials_tags AS tt ON tt.tutorial = t.id LEFT JOIN tags ON tags.id = tt.tag WHERE ${WHERE_VISIBLE_ACTIVE} AND f.userid = ? GROUP BY t.id ORDER BY created DESC`, [req.params.userid])
    .then((tutorials) => {
        res.send(tutorials);
    })
    .catch((err) => {
        res.sendError(500, err);
    });
});

router.post('/favorite', function(req, res) {

    mysqlLib.getConnection().query("INSERT IGNORE INTO favorites (tutorialid, userid) VALUES (?, ?)", [
        req.body.tutorialid, req.body.userid
    ])
    .then((result) => {
        res.sendResult(1, "Dodano do listy ulubionych.");
    })
    .catch((err) => {
        res.sendResult(0, "Wystąpił nieoczekiwany błąd...");
    });
});

router.delete('/favorite/:userid/:tutorialid', function(req, res) {

    mysqlLib.getConnection().query("DELETE FROM favorites WHERE userid = ? AND tutorialid = ?", [
        req.params.userid, req.params.tutorialid
    ])
    .then((result) => {
        res.sendResult(1, "Usunięto z listy ulubionych.");
    })
    .catch((err) => {
        res.sendResult(0, "Wystąpił nieoczekiwany błąd...");
    });
});



module.exports = router;