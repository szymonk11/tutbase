const express = require('express');
const mysqlLib = require('../mysqlLib');

const router = express.Router();


router.get('/top/:cnt', function(req, res) {

    //mysqlLib.getConnection().query("SELECT * FROM tags ORDER BY tutorials_cnt DESC LIMIT ?", [parseInt(req.params.cnt)])
    mysqlLib.getConnection().query("SELECT t.id, t.name, COUNT(tt.tutorial) as tutorials_cnt FROM tutorials_tags AS tt INNER JOIN tags AS t ON t.id = tt.tag" 
        +" INNER JOIN tutorials AS tut ON tut.id = tt.tutorial"
        +" WHERE tut.visible = 1 AND tut.status = 3"
        +" GROUP BY t.id ORDER BY tutorials_cnt DESC LIMIT ?", [parseInt(req.params.cnt)])
    .then((tags) => {
        res.send(tags);
    })
    .catch((err) => {
        res.sendError(500, err);
    });

});

module.exports = router;