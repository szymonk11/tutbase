const express = require('express');
const mysqlLib = require('../mysqlLib');

const router = express.Router();

router.get('/statistics', function(req, res) {

    mysqlLib.getConnection().query(`SELECT u.id AS newest_user_id, u.username AS newest_user_username, users_cnt_tb.cnt AS users_cnt, active_tutorials_tb.cnt AS active_tutorials_cnt FROM users AS u, (SELECT COUNT(id) AS cnt FROM users) users_cnt_tb, (SELECT COUNT(*) AS cnt FROM tutorials t WHERE t.status = 3 AND t.visible = 1) AS active_tutorials_tb ORDER BY u.id DESC LIMIT 1`)
    .then((tutorials) => {
        
        res.sendResult(1, "Okay", tutorials[0]);
    })
    .catch((err) => {
        res.sendError(500, err);
    });

});

module.exports = router;
