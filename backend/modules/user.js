const express = require('express');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const CONFIG = require('../config');
const mysqlLib = require('../mysqlLib');
const mailer = require('../mailer');
const { resolveModuleName } = require('typescript');

const router = express.Router();

function makeid(length) {
    var result = [];
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result.push(characters.charAt(Math.floor(Math.random() *
            charactersLength)));
    }
    return result.join('');
}

router.post('/login', (req, res) => {

    let email = req.body.email;
    let password = req.body.password;
    let remember = req.body.remember;

    mysqlLib.getConnection().query("SELECT * from users WHERE email = ?", [ email ])
    .then((result) => {
        if(result.length > 0) {
            let user = result.shift();

            bcrypt.compare(password, user.password, function(err, same) {
                if(same) {

                    if(user.confirmed == 1) {

                        let ip = req.ip;
                        let agent = req.get('User-Agent');

                        jwt.sign({
                            data: { id: user.id, email: user.email, agent: agent, ip: ip }
                        }, CONFIG.secretkey, {
                            expiresIn: '8h'
                        }, 
                        (err, token) => {

                            if(!err && token) {
                                
                                if(remember == 1) {
                                    jwt.sign({
                                        data: { id: user.id, email: user.email, agent: agent, ip: ip }
                                    }, CONFIG.secretkey, {
                                        expiresIn: '14d'
                                    },
                                    (err, remember_token) => {

                                        if(!err && remember_token) {

                                            let result = {
                                                token: token,
                                                user: {
                                                    id: user.id,
                                                    username: user.username,
                                                    email: user.email,
                                                    regdate: user.regdate,
                                                    isadmin: user.admin == 1
                                                },
                                                remember_token: remember_token
                                            };
                                            mysqlLib.getConnection().query("UPDATE users SET lastlogin = CURRENT_TIMESTAMP(), lastip = ?, lastuseragent = ?, remember = ? WHERE id = ?", [ip, agent, remember_token, user.id]);
                                            res.sendResult(1, "Zalogowano pomyślnie!", result);
                                            return;

                                        } else {
                                            res.sendError(500, err);
                                        }
                                    });

                                } else {
                                    let result = {
                                        token: token,
                                        user: {
                                            id: user.id,
                                            username: user.username,
                                            email: user.email,
                                            regdate: user.regdate,
                                            isadmin: user.admin == 1
                                        }
                                    };
                                    mysqlLib.getConnection().query("UPDATE users SET remember = NULL, lastlogin = CURRENT_TIMESTAMP(), lastip = ?, lastuseragent = ? WHERE id = ?", [ip, agent, user.id]);
                                    res.sendResult(1, "Zalogowano pomyślnie!", result);
                                }
                            
                            } else {
                                res.sendError(500, err);
                            }
                        });
                    } else {
                        res.sendResult(0, "Konto jest nieaktywne.");
                    }
                    
                } else {
                    res.sendResult(0, "Podany login lub hasło są niepoprawne.");
                }
            });
        } else {
            res.sendResult(0, "Podany login lub hasło są niepoprawne.");
        }
    })
    .catch((err) => {
        res.sendError(500, err);
    });
});

router.post('/isLoggedIn', function (req, res) {
    let authorized = req.authorizeToken();
    if(authorized) {

        if(authorized.renew_token) {

            let ip = req.ip;
            let agent = req.get('User-Agent');

            jwt.sign({
                data: { id: authorized.id, email: authorized.email, agent: agent, ip: ip }
            }, CONFIG.secretkey, {
                expiresIn: '8h'
            }, 
            (err, token) => {

                if(!err && token) {
                                 
                    let result = {
                        id: authorized.id,
                        username: authorized.username,
                        email: authorized.email,
                        token: token
                    };   

                    res.sendResult(1, "Jesteś już zalogowany!", result);
                    return;
                        
                } else {
                    res.sendError(500, err);
                }
            });
        } else {
            res.sendResult(1, "Jesteś już zalogowany!", authorized);
        }

    } else {
        res.sendResult(0, "Twoja sesja wygasła. Zaloguj się ponownie!");
    }
});

router.post('/isAdmin', function (req, res) {
    req.isAdmin((is) => {
        if(is) {
            res.sendResult(1, "", 1);
        } else {
            res.sendResult(0, "Nie masz wystarczających uprawnień!");
        }
    });
});

router.post('/logout', function (req, res) {
    let authorized = req.authorizeToken(req.body.token);
    if(authorized) {
        
        mysqlLib.getConnection().query("UPDATE users SET remember = NULL WHERE id = ?", [ authorized.id ]);
    }

    res.send();
});

router.post('/register', (req, res) => {

    let email = req.body.email;
    let password = req.body.password;
    let name = req.body.name;

    // check if user exists
    mysqlLib.getConnection().query("SELECT * from users WHERE email = ?", [ email ])
    .then((result) => {
        if(result.length > 0) {
            res.sendResult(0, "Podany adres e-mail jest już zajęty.");
            return;
        }

        bcrypt.genSalt(10, (err, salt) => {
            if(err) {
                res.sendResult(0, "Coś poszło nie tak...");
                return;
            }
    
            bcrypt.hash(password, salt, (err, hash) => {
                if(err) {
                    res.sendResult(0, "Coś poszło nie tak...");
                    return;
                }
    
                let regip = req.ip;

                let register_key = makeid(96);
    
                mysqlLib.getConnection().query("INSERT INTO users (username, password, salt, email, regip, register_key) VALUES (?, ?, ?, ?, ?, ?)", [
                    name, hash, salt, email, regip, register_key
                ])
                .then((result) => {

                    let userid = result.insertId;
                    mailer.sendRegisterMail(email, userid, register_key);
                    res.sendResult(1, "Rejestracja przebiegła pomyślnie! Sprawdź swoją skrzynkę pocztową, aby potwierdzić konto.");
                })
                .catch((err) => {
                    res.sendResult(0, "Wystąpił błąd podczas rejestracji. Spróbuj jeszcze raz!");
                    return;
                });
            });
        });
    })
    .catch((err) => {
        res.sendResult(0, "Wystąpił błąd podczas rejestracji. Spróbuj jeszcze raz!");
        return;
    });
});

router.post('/confirm', (req, res) => {
    let userid = req.body.userid;
    let register_key = req.body.register_key;

    mysqlLib.getConnection().query("SELECT * FROM users WHERE id = ? AND register_key = ? AND confirmed = 0", [ userid, register_key ])
    .then((result) => {
        if(result.length <= 0) {
            res.sendResult(0, "Podano błędne dane.");
            return;
        }

        mysqlLib.getConnection().query("UPDATE users SET confirmed = 1 WHERE id = ?", [ userid ])
        .then((result) => {
            res.sendResult(1, "Twoje konto zostało pomyślnie aktywowane.");
        })
        .catch((err) => {
            res.sendResult(0, "Wystąpił nieoczekiwany błąd. Spróbuj jeszcze raz!");
            return;
        });

    })
    .catch((err) => {
        res.sendResult(0, "Wystąpił nieoczekiwany błąd. Spróbuj jeszcze raz!");
        return;
    });   
});

router.post('/changepassword', (req, res) => {

    let userid = req.body.userid;
    let password = req.body.password;
    let newPassword = req.body.newpassword;

    let authorized = req.authorizeToken();
    if(authorized) {
        
        mysqlLib.getConnection().query("SELECT * from users WHERE id = ?", [ userid ])
        .then((result) => {
            if(result.length > 0) {
                let user = result.shift();

                bcrypt.compare(password, user.password, function(err, same) {
                    if(same) {

                        bcrypt.genSalt(10, (err, salt) => {
                            if(err) {
                                res.sendResult(0, "Coś poszło nie tak...");
                                return;
                            }
                    
                            bcrypt.hash(newPassword, salt, (err, hash) => {
                                if(err) {
                                    res.sendResult(0, "Coś poszło nie tak...");
                                    return;
                                }

                                mysqlLib.getConnection().query("UPDATE users SET password = ?, salt = ? WHERE id = ?", [hash, salt, user.id]);

                                res.sendResult(1, "Hasło zostało pomyślnie zmienione!");
                            });
                        });
                        
                    } else {
                        res.sendResult(0, "Podany login lub hasło są niepoprawne.");
                    }
                });
            } else {
                res.sendResult(0, "Podany login lub hasło są niepoprawne.");
            }
        })
        .catch((err) => {
            res.sendError(500, err);
        });
    
    } else {
        res.sendResult(0, "Twoja sesja wygasła. Zaloguj się ponownie!");
    }
});


router.post('/remindpassword', (req, res) => {

    let email = req.body.email;

    // check if user exists
    mysqlLib.getConnection().query("SELECT * from users WHERE email = ?", [ email ])
    .then((result) => {
        if(result.length <= 0) {
            res.sendResult(0, "Nie odnaleziono konta o podanym adresie e-mail.");
            return;
        }

        let userid = result[0].id;
        let reset_key =  makeid(96);
        let reset_time = Date.now();
    
        mysqlLib.getConnection().query("UPDATE users SET reset_key = ?, reset_time = ? WHERE id = ?", [
           reset_key, reset_time, userid
        ])
        .then((result) => {
            mailer.sendResetPasswordMail(email, userid, reset_key);
            res.sendResult(1, "Przypomnienie hasła zostało wysłane na podany adres e-mail. Sprawdź swoją skrzynkę!");
        })
        .catch((err) => {
            res.sendResult(0, "Wystąpił błąd. Spróbuj jeszcze raz!");
            return;
        });
    })
    .catch((err) => {
        res.sendResult(0, "Wystąpił błąd. Spróbuj jeszcze raz!");
        return;
    });
});

router.post('/resetpassword', (req, res) => {

    let userid = req.body.userid;
    let reset_key = req.body.reset_key;
    let newPassword = req.body.newPassword;

    mysqlLib.getConnection().query("SELECT * from users WHERE id = ?", [ userid ])
    .then((result) => {
        if(result.length > 0) {
            let user = result.shift();

            let reset_time_max = Date.now() - 1000*60*15; // 15 minut

            if(user.reset_key != reset_key || user.reset_time < reset_time_max) {
                res.sendResult(0, "Link do zmiany hasła wygasł lub jest nieprawidłowy.");
                return;
            }

            bcrypt.genSalt(10, (err, salt) => {
                if(err) {
                    res.sendResult(0, "Coś poszło nie tak...");
                    return;
                }
        
                bcrypt.hash(newPassword, salt, (err, hash) => {
                    if(err) {
                        res.sendResult(0, "Coś poszło nie tak...");
                        return;
                    }

                    mysqlLib.getConnection().query("UPDATE users SET password = ?, salt = ?, reset_key = NULL WHERE id = ?", [hash, salt, user.id]);

                    res.sendResult(1, "Hasło zostało pomyślnie zmienione!");
                });
            });

        } else {
            res.sendResult(0, "Link do zmiany hasła wygasł lub jest nieprawidłowy.")
        }
    })
    .catch((err) => {
        res.sendError(500, err);
    });
});

router.get('/preview/:userid', (req, res) => {

    mysqlLib.getConnection().query("SELECT id, username, regdate FROM users WHERE id = ? LIMIT 1", [req.params.userid])
    .then((users) => {

        if(!users || users.length < 1) {
            res.sendError(404, false, "Nie odnaleziono użytkownika.");
            return;
        }

        let user = users.shift()

        res.send(user);
        return;

    }).catch((err) => {
        res.sendError(500, err);
        return;
    });

});

// define the home page route
/*router.get('/:pass', function (req, res) {
    
    bcrypt.genSalt(10, (err, salt) => {

        if(err) {
            
        };

        bcrypt.hash(req.params.pass, salt, (err, hash) => {

            res.send(`Salt: ${salt}<br />Hash: ${hash}`);

        });

    });
});*/

module.exports = router;