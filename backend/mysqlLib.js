const mariadb = require('mariadb');

const pool = mariadb.createPool({
    host: '185.41.68.195', 
    user:'tutbase', 
    password: 'YYQZEakV3mB6AMmv',
    database: 'tutbase'
});

var connection = null;

function connect()
{
    pool.getConnection()
    .then(conn => {
        connection = conn;
        console.log("DATABASE CONNECTION SUCCESS");
        
        connection.on('error', err => {
            console.log(err);
            console.log("DATABASE RECONNECTING...");
            connect();
        });
    })
    .catch(err => {
        console.log(err);
        
        setTimeout(() => {
            console.log("DATABASE RECONNECTING IN 5 seconds...");
            connect();
        }, 5000);
    });
}
connect();

exports.getConnection = () => connection;